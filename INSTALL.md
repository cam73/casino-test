# Установка проекта
Онлайн казино (тестовое задание)

## Содержание
* [Развертывание и разработка](#deployment)
* [Работа с проектом](#support)

## <a name="deployment"></a>Развёртывание проекта с помощью Docker
1. Склонировать проект;
1. Зайти в папку `/docker`;
1. Создать собственный файл окружения `.env` на основе файла `.env.dist`;
1. Создать контейнеры `docker-compose up -d`;
1. Зайти в контейнер `docker-compose exec --user=app app bash`;
1. Установить composer-зависимости `composer install --no-interaction`;
1. Создать схему базы данных `php ./bin/console doctrine:database:create`;
1. Выполнить миграции `php bin/console doctrine:migrations:migrate`;
1. При необходимости установить дамп с тестовыми данными `/doc/dump/test_data.sql`;
1. Создать собственный файл `phpunit.xml` в корне файла на основе `phpunit.xml.dist`;
1. Убедиться, что все тесты выполняются `php vendor/phpunit/phpunit/phpunit --configuration phpunit.xml`;

## <a name="support"></a>Работа с проектом
- Команда запуска публикации задач на отправку денежных транзакций в банк:
 ```
 php bin/console app:money-transactions:publish --max-jobs=10
```

- Команда (консюмер) для обработки задач в очереди на отправку денежных транзакций в банк:
```
	php bin/console enqueue:consume --setup-broker
```

- Команда генерации хэша для пользовательского пароля:
```
    php bin/console security:encode-password
```
