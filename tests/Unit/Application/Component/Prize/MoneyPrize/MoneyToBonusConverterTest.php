<?php

namespace Tests\Unit\Application\Component\Prize;

use Application\Component\Prize\MoneyPrize\MoneyToBonusConverter;
use Assert\AssertionFailedException;
use PHPUnit\Framework\TestCase;

class MoneyToBonusConverterTest extends TestCase
{
    /**
     * @param int $moneyPerOneBonus
     * @throws AssertionFailedException
     *
     * @dataProvider failedConstructProvider()
     */
    public function testFailedConstruct(int $moneyPerOneBonus)
    {
        $this->expectException(AssertionFailedException::class);
        new MoneyToBonusConverter($moneyPerOneBonus);
    }

    public function failedConstructProvider(): array
    {
        return [
            'moneyPerOneBonus is zero' => [
                'moneyPerOneBonus' => 0,
            ],
            'moneyPerOneBonus less than zero' => [
                'moneyPerOneBonus' => -5,
            ]
        ];
    }

    /**
     * @param string $moneyAmount
     * @param int $moneyPerOneBonus
     * @param string $expectedResult
     * @throws AssertionFailedException
     *
     * @dataProvider convertProvider()
     */
    public function testConvert(string $moneyAmount, int $moneyPerOneBonus, string $expectedResult)
    {
        $converter = new MoneyToBonusConverter($moneyPerOneBonus);
        expect($converter->convert($moneyAmount))->equals($expectedResult);
    }

    public function convertProvider(): array
    {
        return [
            [
                'moneyAmount' => '100',
                'moneyPerOneBonus' => 25,
                'expectedResult' => '4',
            ],
            [
                'moneyAmount' => '77',
                'moneyPerOneBonus' => 1,
                'expectedResult' => '77',
            ],
            [
                'moneyAmount' => '7',
                'moneyPerOneBonus' => 2,
                'expectedResult' => '4',
            ],
        ];
    }

}
