<?php

namespace Framework\Controller\Balance;

use Application\Feature\Fund\UseCase\ShowBalance\Interactor as CasinoFundShowBalanceInteractor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CasinoFundController extends AbstractController
{
    private CasinoFundShowBalanceInteractor $casinoFundShowBalanceInteractor;

    public function __construct(
        CasinoFundShowBalanceInteractor $casinoFundShowBalanceInteractor
    )
    {
        $this->casinoFundShowBalanceInteractor = $casinoFundShowBalanceInteractor;
    }

    /**
     * @Route(path="/casino/fund", name="casino_fund")
     */
    public function casinoFund()
    {
        return $this->render('casino/fund.html.twig', [
            'view' => $this->casinoFundShowBalanceInteractor->getView(),
        ]);
    }
}
