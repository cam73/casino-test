<?php

namespace Framework\Controller\Balance;

use Application\Feature\Bonus\UseCase\ShowBalance\Interactor as BonusShowBalanceInteractor;
use Framework\Database\Entity\ORMUser;
use Framework\Factory\DomainClientFactory;
use Framework\Database\Repository\ORMClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BonusBalanceController extends AbstractController
{
    private BonusShowBalanceInteractor $bonusShowBalanceInteractor;

    public function __construct(
        BonusShowBalanceInteractor $bonusShowBalanceInteractor
    )
    {
        $this->bonusShowBalanceInteractor = $bonusShowBalanceInteractor;
    }

    /**
     * @Route(path="/bonus/balance", name="bonus_balance")
     */
    public function bonusBalance()
    {
        return $this->render('bonus/balance.html.twig', [
            'view' => $this->bonusShowBalanceInteractor->getView($this->getUser()->getId()),
        ]);
    }
}
