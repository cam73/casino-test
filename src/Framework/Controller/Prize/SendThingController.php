<?php

namespace Framework\Controller\Prize;

use Application\Feature\Prize\UseCase\SendThing\Interactor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class SendThingController extends AbstractController
{
    private Interactor $sendThingInteractor;

    public function __construct(
        Interactor $sendThingInteractor
    ) {
        $this->sendThingInteractor = $sendThingInteractor;
    }

    /**
     * @Route(
     *     path="/prize/send-thing/{thingPrizeId}",
     *     name="prize_send_thing",
     * )
     * @param int $thingPrizeId
     * @return RedirectResponse
     */
    public function sendThing(int $thingPrizeId)
    {
        $this->sendThingInteractor->sendThingPrize($thingPrizeId);

        $this->addFlash('notice', 'Товар отправлен.');

        return $this->redirectToRoute('prize_list');
    }
}
