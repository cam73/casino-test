<?php

namespace Framework\Controller\Prize;

use Exception;
use Framework\Form\DTO\BankAccount;
use Framework\Form\Type\BankAccountType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Application\Feature\Prize\UseCase\ScheduleTransferMoney\Interactor;

class TransferMoneyController  extends AbstractController
{
    private Interactor $interactor;

    public function __construct(Interactor $interactor)
    {
        $this->interactor = $interactor;
    }

    /**
     * @Route(
     *     path="/prize/transfer-money/{moneyPrizeId}",
     *     name="prize_transfer_money",
     * )
     * @param int $moneyPrizeId
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function transferMoney(int $moneyPrizeId, Request $request): Response
    {
        $form = $this->createForm(BankAccountType::class, new BankAccount('', ''));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var BankAccount $bankAccount */
            $bankAccount = $form->getData();

            $this->interactor->scheduleTransferMoney(
                $moneyPrizeId,
                $bankAccount->getPayeeBankTin(),
                $bankAccount->getPayeeAccountNumber()
            );

            $this->addFlash('notice','Денежный трансфер запланирован.');

            return $this->redirectToRoute('prize_list');
        }

        return $this->render('prize/transfer_money.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
