<?php

namespace Framework\Controller\Prize;

use Application\Feature\Prize\UseCase\ShowList\Interactor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    private Interactor $showListInteractor;

    public function __construct(Interactor $showListInteractor)
    {
        $this->showListInteractor = $showListInteractor;
    }

    /**
     * @Route(path="/prize/list", name="prize_list")
     */
    public function list()
    {
        $moneyPerOneBonus = $this->getParameter('money_per_one_bonus');

        return $this->render('prize/list.html.twig', [
            'view' => $this->showListInteractor->getView($this->getUser()->getId()),
            'moneyPerOneBonus' => $moneyPerOneBonus,
        ]);
    }

}
