<?php

namespace Framework\Controller\Prize;

use Application\Feature\Prize\UseCase\GiveRandomPrize\Interactor as GiveRandomPrizeInteractor;
use Framework\Form\Type\PrizeRandomType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GetRandomController extends AbstractController
{
    private GiveRandomPrizeInteractor $giveRandomPrizeInteractor;

    public function __construct(GiveRandomPrizeInteractor $giveRandomPrizeInteractor)
    {
        $this->giveRandomPrizeInteractor = $giveRandomPrizeInteractor;
    }

    /**
     * @Route(path="/prize/random", name="prize_random")
     * @param Request $request
     * @return Response
     */
    public function getRandom(Request $request): Response
    {
        $form = $this->createForm(PrizeRandomType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $view = $this->giveRandomPrizeInteractor->giveRandomPrize($this->getUser()->getId());

            $this->addFlash(
                'notice',
                sprintf('Получите подарок! %s', $view->getTitle())
            );

            return $this->redirectToRoute('prize_list');
        }

        return $this->render('prize/get_random.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
