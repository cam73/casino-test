<?php

namespace Framework\Controller\Prize;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Application\Feature\Prize\UseCase\ConvertMoneyPrizeToBonus\Interactor;

class ConvertMoneyToBonusController  extends AbstractController
{
    private Interactor $interactor;

    public function __construct(Interactor $interactor)
    {
        $this->interactor = $interactor;
    }

    /**
     * @Route(
     *     path="/prize/convert-money-to-bonus/{moneyPrizeId}",
     *     name="prize_convert_money_to_bonus",
     * )
     * @param int $moneyPrizeId
     * @return Response
     */
    public function transferMoney(int $moneyPrizeId): Response
    {
        $this->interactor->convertMoneyPrizeToBonus($moneyPrizeId);

        $this->addFlash('notice','Денежный приз сконвертирован по курсу .');

        return $this->redirectToRoute('prize_list');
    }
}
