<?php

namespace Framework\Controller\Prize;

use Application\Feature\Prize\UseCase\Decline\Interactor as DeclineInteractor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class DeclineController extends AbstractController
{
    private DeclineInteractor $declineInteractor;

    public function __construct(
        DeclineInteractor $declineInteractor
    )
    {
        $this->declineInteractor = $declineInteractor;
    }

    /**
     * @Route(
     *     path="/prize/decline/{type}/{id}",
     *     name="prize_decline",
     *     requirements={"type": "bonus|money|thing"}
     * )
     * @param string $type
     * @param int $id
     * @return RedirectResponse
     */
    public function decline(string $type, int $id): RedirectResponse
    {
        $this->declineInteractor->declinePrize($type, $id);

        $this->addFlash('notice', 'Приз отклонён');

        return $this->redirectToRoute('prize_list');
    }
}
