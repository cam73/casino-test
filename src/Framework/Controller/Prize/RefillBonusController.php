<?php

namespace Framework\Controller\Prize;

use Application\Feature\Prize\UseCase\RefillBonus\Interactor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class RefillBonusController extends AbstractController
{
    private Interactor $bonusRefillInteractor;

    public function __construct(
        Interactor $bonusRefillInteractor
    ) {
        $this->bonusRefillInteractor = $bonusRefillInteractor;
    }

    /**
     * @Route(
     *     path="/prize/bonus-refill/{bonusPrizeId}",
     *     name="prize_bonus_refill",
     * )
     * @param int $bonusPrizeId
     * @return RedirectResponse
     */
    public function bonusRefill(int $bonusPrizeId)
    {
        $this->bonusRefillInteractor->refillBonus($bonusPrizeId);

        $this->addFlash('notice', 'Бонусы начислены.');

        return $this->redirectToRoute('bonus_balance');
    }
}
