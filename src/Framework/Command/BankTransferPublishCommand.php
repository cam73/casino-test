<?php

namespace Framework\Command;

use Enqueue\Client\Message;
use Enqueue\Client\ProducerInterface;
use Framework\Database\Repository\ORMBankTransferRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BankTransferPublishCommand extends Command
{
    private const DEFAULT_MAX_JOBS = 7;

    private ORMBankTransferRepository $bankTransferRepository;

    private ProducerInterface $producer;

    protected static $defaultName = 'app:money-transactions:publish';

    public function __construct(ORMBankTransferRepository $bankTransferRepository, ProducerInterface $producer)
    {
        $this->bankTransferRepository = $bankTransferRepository;
        $this->producer = $producer;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Publish money transactions for transfer to bank')
            ->addOption('max-jobs', null, InputOption::VALUE_OPTIONAL, 'Number of jobs to send money transactions')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Started.');

        $maxJobs = self::DEFAULT_MAX_JOBS;
        if ($input->getOption('max-jobs')) {
            $maxJobs = $input->getOption('max-jobs');
        }

        $output->writeln(sprintf('Max jobs is %s', $maxJobs));

        $bankTransfers = $this->bankTransferRepository->findForSendToBank($maxJobs);

        foreach ($bankTransfers as $bankTransfer) {
            $message = new Message($bankTransfer->getId(), [], []);
            $this->producer->sendEvent('transfer_money_to_bank', $message);
        }

        $output->writeln(sprintf('Sent %s transactions.', count($bankTransfers)));

        $output->writeln('Finished.');
        return 0;
    }
}
