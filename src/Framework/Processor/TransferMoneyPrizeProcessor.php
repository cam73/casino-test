<?php

namespace Framework\Processor;

use Enqueue\Client\TopicSubscriberInterface;
use Application\Feature\Prize\UseCase\TransferMoney\Interactor;
use Interop\Queue\Context;
use Interop\Queue\Message;
use Interop\Queue\Processor;

class TransferMoneyPrizeProcessor implements Processor, TopicSubscriberInterface
{
    private Interactor $interactor;

    public function __construct(Interactor $interactor)
    {
        $this->interactor = $interactor;
    }

    public function process(Message $message, Context $context)
    {
        try {
            $this->interactor->transferMoney($message->getBody());

        } catch (\Exception $e) {
            return self::REQUEUE;
        }

        return self::ACK;
    }

    public static function getSubscribedTopics()
    {
        return ['transfer_money_to_bank'];
    }
}
