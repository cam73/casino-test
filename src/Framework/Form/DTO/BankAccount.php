<?php

namespace Framework\Form\DTO;

class BankAccount
{
    private string $payeeBankTin;

    private string $payeeAccountNumber;

    public function __construct(string $payeeBankTin, string $payeeAccountNumber)
    {
        $this->payeeBankTin = $payeeBankTin;
        $this->payeeAccountNumber = $payeeAccountNumber;
    }

    public function getPayeeBankTin(): string
    {
        return $this->payeeBankTin;
    }

    public function getPayeeAccountNumber(): string
    {
        return $this->payeeAccountNumber;
    }

    public function setPayeeBankTin(string $payeeBankTin): void
    {
        $this->payeeBankTin = $payeeBankTin;
    }

    public function setPayeeAccountNumber(string $payeeAccountNumber): void
    {
        $this->payeeAccountNumber = $payeeAccountNumber;
    }
}
