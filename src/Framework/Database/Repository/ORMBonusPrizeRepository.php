<?php

namespace Framework\Database\Repository;

use Assert\Assertion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Framework\Database\Entity\ORMBonusPrize;
use Framework\Database\Entity\ORMBonusTransaction;
use Framework\Database\Entity\ORMClient;

class ORMBonusPrizeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ORMBonusPrize::class);
    }

    /**
     * @param ORMClient $client
     * @return array|ORMBonusPrize[]
     */
    public function findBonusPrizes(ORMClient $client): array
    {
        return $this->findBy([
            'client' => $client,
        ]);
    }

    public function registryRandomPrize(ORMClient $client, int $maxSize): ORMBonusPrize
    {
        $newBonusTransaction = new ORMBonusTransaction(
            $client,
            rand(1, $maxSize),
            'receipt',
            'reserved'
        );

        return new ORMBonusPrize(
            $client,
            $newBonusTransaction,
            'available'
        );
    }
}
