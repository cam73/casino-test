<?php

namespace Framework\Database\Repository;

use Assert\Assertion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Framework\Database\Entity\ORMClient;
use Framework\Database\Entity\ORMThing;
use Framework\Database\Entity\ORMThingPrize;

class ORMThingPrizeRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ORMThingPrize::class);
    }

    /**
     * @param ORMClient $client
     * @return array|ORMThingPrize[]
     */
    public function findThingPrizes(ORMClient $client): array
    {
        return $this->findBy([
            'client' => $client,
        ]);
    }

    public function registryPrize(ORMClient $client, ORMThing $thing): ORMThingPrize
    {
        $thing->setStatus(ORMThing::STATUS_RESERVED);
        $thing->setClient($client);

        return new ORMThingPrize(
            $client,
            $thing,
            'available'
        );
    }
}
