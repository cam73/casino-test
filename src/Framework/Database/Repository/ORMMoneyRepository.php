<?php

namespace Framework\Database\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Framework\Database\Entity\ORMMoneyTransaction;

class ORMMoneyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ORMMoneyTransaction::class);
    }

    public function getFundMoneyBalanceAmount(): string
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = <<<SQL
            SELECT
                SUM(`amount` * IF(`type` IN ('receipt'), 1, -1)) AS `amount`
            FROM `money_transaction`
            WHERE
                `status` IN ('actively', 'reserved') &&
                `own_of_fund`
        SQL;
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetch()['amount'];
    }

}
