<?php

namespace Framework\Database\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Framework\Database\Entity\ORMBonusTransaction;
use Framework\Database\Entity\ORMClient;

class ORMBonusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ORMBonusTransaction::class);
    }

    public function getBonusBalanceAmount(ORMClient $client): string
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = <<<SQL
            SELECT
                SUM(`amount` * IF(`type` IN ('receipt'), 1, -1)) AS `amount`
            FROM bonus_transaction
            WHERE
                `status` IN ('actively') &&
                `client_id` = :clientId
        SQL;
        $stmt = $conn->prepare($sql);
        $stmt->execute(['clientId' => $client->getId()]);

        $result = $stmt->fetch()['amount'];
        
        return is_null($result)?0:$result;
    }

}
