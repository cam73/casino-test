<?php

namespace Framework\Database\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Framework\Database\Entity\ORMBankTransfer;

class ORMBankTransferRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ORMBankTransfer::class);
    }

    /**
     * @param int $max
     * @return ORMBankTransfer[]
     */
    public function findForSendToBank(int $max): array
    {
        return $this->findBy(['state' => ORMBankTransfer::STATE_WAIT_FOR_SEND], null, $max);
    }
}
