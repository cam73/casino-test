<?php

namespace Framework\Database\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Framework\Database\Entity\ORMThing;

class ORMThingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ORMThing::class);
    }

    /**
     * @return array|ORMThing[]
     */
    public function findFundThings(): array
    {
        return $this->findBy([
            'client' => null,
            'ownOfFund' => true,
            'status' => 'actively',
         ]);
    }

    public function getFundThingsAmount(): string
    {
        return $this->count(['status' => 'actively']);
    }

    public function getRandomThing(): ORMThing
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = <<<SQL
            SELECT
                id
            FROM `thing`
            WHERE
                `status` IN ('actively')
            ORDER BY RAND() ASC
            LIMIT 1
        SQL;
        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $this->find($stmt->fetch()['id']);
    }
}
