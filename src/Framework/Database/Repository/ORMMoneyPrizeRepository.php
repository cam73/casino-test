<?php

namespace Framework\Database\Repository;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Framework\Database\Entity\ORMClient;
use Framework\Database\Entity\ORMMoneyPrize;
use Framework\Database\Entity\ORMMoneyTransaction;

class ORMMoneyPrizeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ORMMoneyPrize::class);
    }

    /**
     * @param ORMClient $client
     * @return array|ORMMoneyPrize[]
     */
    public function findMoneyPrizes(ORMClient $client): array
    {
        return $this->findBy([
            'client' => $client,
        ]);
    }

    /**
     * @param ORMClient $client
     * @param int $maxSize
     * @return ORMMoneyPrize
     * @throws AssertionFailedException
     */
    public function registryRandomPrize(ORMClient $client, int $maxSize): ORMMoneyPrize
    {
        $newMoneyTransaction = new ORMMoneyTransaction(
            $client,
            true,
            rand(1, $maxSize),
            'expense',
            'reserved'
        );

        $newMoneyPrize = new ORMMoneyPrize();
        $newMoneyPrize->setClient($client);
        $newMoneyPrize->setMoneyTransaction($newMoneyTransaction);
        $newMoneyPrize->setStatus(ORMMoneyPrize::STATUS_AVAILABLE);

        return $newMoneyPrize;
    }
}
