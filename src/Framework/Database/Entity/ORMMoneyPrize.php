<?php

namespace Framework\Database\Entity;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity()
 * @ORM\Table(name="money_prize")
 */
class ORMMoneyPrize
{
    public const STATUS_AVAILABLE = 'available';
    public const STATUS_WAIT_FOR_TRANSFER = 'wait_for_transfer';
    public const STATUS_TRANSFERRED = 'transferred';
    public const STATUS_CONVERTED_TO_BONUS = 'converted_to_bonus';
    public const STATUS_DECLINED = 'declined';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ManyToOne(targetEntity="ORMClient", cascade={"persist"})
     * @JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    private ORMClient $client;

    /**
     * @ManyToOne(targetEntity="ORMMoneyTransaction", cascade={"persist"})
     * @JoinColumn(name="money_transaction_id", referencedColumnName="id", nullable=false)
     */
    private ORMMoneyTransaction $moneyTransaction;

    /**
     * @ORM\Column(
     *     type="string",
     *     columnDefinition="ENUM('available', 'wait_for_transfer', 'transferred', 'converted_to_bonus', 'declined')",
     *     nullable=false,
     *     options={"default": "available"}
     * )
     */
    private string $status;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getClient(): ORMClient
    {
        return $this->client;
    }

    public function setClient(ORMClient $client): void
    {
        $this->client = $client;
    }

    public function getMoneyTransaction(): ORMMoneyTransaction
    {
        return $this->moneyTransaction;
    }

    public function setMoneyTransaction(ORMMoneyTransaction $moneyTransaction): void
    {
        $this->moneyTransaction = $moneyTransaction;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @throws AssertionFailedException
     */
    public function setStatus(string $status): void
    {
        Assertion::inArray($status, [
            self::STATUS_AVAILABLE,
            self::STATUS_WAIT_FOR_TRANSFER,
            self::STATUS_TRANSFERRED,
            self::STATUS_CONVERTED_TO_BONUS,
            self::STATUS_DECLINED,
        ]);

        $this->status = $status;
    }
}
