<?php

namespace Framework\Database\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * @ORM\Entity()
 * @ORM\Table(name="client")
 */
class ORMClient
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @OneToOne(targetEntity="ORMUser", inversedBy="client")
     * @JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private ORMUser $user;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getUser(): ORMUser
    {
        return $this->user;
    }

    public function setUser(ORMUser $user): void
    {
        $this->user = $user;
    }
}
