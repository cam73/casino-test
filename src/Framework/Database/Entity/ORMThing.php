<?php

namespace Framework\Database\Entity;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity()
 * @ORM\Table(name="thing")
 */
class ORMThing
{
    public const STATUS_RESERVED = 'reserved';
    public const STATUS_ACTIVELY = 'actively';
    public const STATUS_TRANSFERRED = 'transferred';
    public const STATUS_CANCELED = 'canceled';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ManyToOne(targetEntity="ORMClient", cascade={"persist"})
     * @JoinColumn(name="client_id", referencedColumnName="id", nullable=true)
     */
    private ?ORMClient $client;

    /**
     * @ORM\Column(type="boolean", name="own_of_fund", options={"default": false})
     */
    private bool $ownOfFund;

    /**
     * @ORM\Column(type="string")
     */
    private string $title;

    /**
     * @ORM\Column(
     *     type="string",
     *     columnDefinition="ENUM('reserved', 'actively', 'transferred', 'canceled')",
     *     nullable=false,
     *     options={"default": "actively"}
     * )
     */
    private string $status;

    public function __construct(?ORMClient $client, bool $ownOfFund, string $title, string $status)
    {
        $this->client = $client;
        $this->ownOfFund = $ownOfFund;
        $this->title = $title;
        $this->status = $status;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return ORMClient|null
     */
    public function getClient(): ?ORMClient
    {
        return $this->client;
    }

    /**
     * @param ORMClient|null $client
     */
    public function setClient(?ORMClient $client): void
    {
        $this->client = $client;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @throws AssertionFailedException
     */
    public function setStatus(string $status): void
    {
        Assertion::inArray($status, [
            self::STATUS_RESERVED,
            self::STATUS_ACTIVELY,
            self::STATUS_TRANSFERRED,
            self::STATUS_CANCELED,
        ]);

        $this->status = $status;
    }

    public function isOwnOfFund(): bool
    {
        return $this->ownOfFund;
    }

    public function setOwnOfFund(bool $ownOfFund): void
    {
        $this->ownOfFund = $ownOfFund;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }
}
