<?php

namespace Framework\Database\Entity;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity()
 * @ORM\Table(name="bonus_transaction")
 */
class ORMBonusTransaction
{
    public const TYPE_RECEIPT = 'receipt';
    public const TYPE_EXPENSE = 'expense';

    public const STATUS_RESERVED = 'reserved';
    public const STATUS_ACTIVELY = 'actively';
    public const STATUS_CANCELED = 'canceled';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ManyToOne(targetEntity="ORMClient", cascade={"persist"})
     * @JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    private ORMClient $client;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=2)
     */
    private string $amount;

    /**
     * @ORM\Column(
     *     type="string",
     *     columnDefinition="ENUM('receipt', 'expense')",
     *     nullable=false
     * )
     */
    private string $type;

    /**
     * @ORM\Column(
     *     type="string",
     *     columnDefinition="ENUM('reserved', 'actively', 'canceled')",
     *     nullable=false,
     *     options={"default": "actively"}
     * )
     */
    private string $status;

    public function __construct(ORMClient $client, string $amount, string $type, string $status)
    {
        $this->client = $client;
        $this->amount = $amount;
        $this->type = $type;
        $this->status = $status;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getClient(): ORMClient
    {
        return $this->client;
    }

    /**
     * @param ORMClient $client
     */
    public function setClient(ORMClient $client): void
    {
        $this->client = $client;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }

    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @throws AssertionFailedException
     */
    public function setType(string $type): void
    {
        Assertion::inArray($type, [
            self::TYPE_RECEIPT,
            self::TYPE_EXPENSE
        ]);

        $this->type = $type;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @throws AssertionFailedException
     */
    public function setStatus(string $status): void
    {
        Assertion::inArray($status, [
            self::STATUS_RESERVED,
            self::STATUS_ACTIVELY,
            self::STATUS_CANCELED
        ]);

        $this->status = $status;
    }
}
