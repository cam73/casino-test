<?php

namespace Framework\Database\Entity;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity()
 * @ORM\Table(name="bonus_prize")
 */
class ORMBonusPrize
{
    public const STATUS_AVAILABLE = 'available';
    public const STATUS_ACCEPTED = 'accepted';
    public const STATUS_DECLINED = 'declined';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ManyToOne(targetEntity="ORMClient", cascade={"persist"})
     * @JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    private ORMClient $client;

    /**
     * @ManyToOne(targetEntity="ORMBonusTransaction", cascade={"persist"})
     * @JoinColumn(name="bonus_transaction_id", referencedColumnName="id", nullable=false)
     */
    private ORMBonusTransaction $bonusTransaction;

    /**
     * @ORM\Column(
     *     type="string",
     *     columnDefinition="ENUM('available', 'accepted', 'declined')",
     *     nullable=false,
     *     options={"default": "available"}
     * )
     */
    private string $status;

    public function __construct(ORMClient $client, ORMBonusTransaction $bonusTransaction, string $status)
    {
        $this->client = $client;
        $this->bonusTransaction = $bonusTransaction;
        $this->status = $status;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getClient(): ORMClient
    {
        return $this->client;
    }

    public function setClient(ORMClient $client): void
    {
        $this->client = $client;
    }

    public function getBonusTransaction(): ORMBonusTransaction
    {
        return $this->bonusTransaction;
    }

    public function setBonusTransaction(ORMBonusTransaction $bonusTransaction): void
    {
        $this->bonusTransaction = $bonusTransaction;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @throws AssertionFailedException
     */
    public function setStatus(string $status): void
    {
        Assertion::inArray($status, [
            self::STATUS_AVAILABLE,
            self::STATUS_ACCEPTED,
            self::STATUS_DECLINED,
        ]);

        $this->status = $status;
    }
}
