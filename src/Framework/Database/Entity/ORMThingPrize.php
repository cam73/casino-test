<?php

namespace Framework\Database\Entity;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ORM\Entity()
 * @ORM\Table(name="thing_prize")
 */
class ORMThingPrize
{
    public const STATUS_AVAILABLE = 'available';
    public const STATUS_TRANSFERRED = 'transferred';
    public const STATUS_DECLINED = 'declined';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ManyToOne(targetEntity="ORMClient", cascade={"persist"})
     * @JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    private ORMClient $client;

    /**
     * @ManyToOne(targetEntity="ORMThing", cascade={"persist"})
     * @JoinColumn(name="thing_id", referencedColumnName="id", nullable=false)
     */
    private ORMThing $thing;

    /**
     * @ORM\Column(
     *     type="string",
     *     columnDefinition="ENUM('available', 'transferred', 'declined')",
     *     nullable=false,
     *     options={"default": "available"}
     * )
     */
    private string $status;

    public function __construct(ORMClient $client, ORMThing $thing, string $status)
    {
        $this->client = $client;
        $this->thing = $thing;
        $this->status = $status;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getClient(): ORMClient
    {
        return $this->client;
    }

    public function setClient(ORMClient $client): void
    {
        $this->client = $client;
    }

    public function getThing(): ORMThing
    {
        return $this->thing;
    }

    public function setThing(ORMThing $thing): void
    {
        $this->thing = $thing;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @throws AssertionFailedException
     */
    public function setStatus(string $status): void
    {
        Assertion::inArray($status, [
            self::STATUS_AVAILABLE,
            self::STATUS_TRANSFERRED,
            self::STATUS_DECLINED,
        ]);

        $this->status = $status;
    }
}
