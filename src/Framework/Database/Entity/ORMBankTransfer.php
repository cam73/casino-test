<?php

namespace Framework\Database\Entity;

use Assert\Assertion;
use Assert\AssertionFailedException;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;

/**
 * @ORM\Entity()
 * @ORM\Table(name="bank_transfer")
 */
class ORMBankTransfer
{
    public const STATE_WAIT_FOR_SEND = 'wait_for_send';
    public const STATE_SENT = 'sent';
    public const STATE_CANCELED = 'canceled';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @OneToOne(targetEntity="ORMMoneyPrize")
     * @JoinColumn(name="money_prize_id", referencedColumnName="id", nullable=false)
     */
    private ORMMoneyPrize $moneyPrize;

    /**
     * @ORM\Column(type="string")
     */
    private string $payeeBankTin;

    /**
     * @ORM\Column(type="string")
     */
    private string $payeeAccountNumber;

    /**
     * @ORM\Column(type="decimal", precision=15, scale=2)
     */
    private string $amount;

    /**
     * @ORM\Column(
     *     type="string",
     *     columnDefinition="ENUM('wait_for_send', 'sent', 'canceled')",
     *     nullable=false,
     *     options={"default": "wait_for_send"}
     * )
     */
    private string $state;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getMoneyPrize(): ORMMoneyPrize
    {
        return $this->moneyPrize;
    }

    public function setMoneyPrize(ORMMoneyPrize $moneyPrize): void
    {
        $this->moneyPrize = $moneyPrize;
    }

    public function getPayeeBankTin(): string
    {
        return $this->payeeBankTin;
    }

    public function setPayeeBankTin(string $payeeBankTin): void
    {
        $this->payeeBankTin = $payeeBankTin;
    }

    public function getPayeeAccountNumber(): string
    {
        return $this->payeeAccountNumber;
    }

    public function setPayeeAccountNumber(string $payeeAccountNumber): void
    {
        $this->payeeAccountNumber = $payeeAccountNumber;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): void
    {
        $this->amount = $amount;
    }

    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @throws AssertionFailedException
     */
    public function setState(string $state): void
    {
        Assertion::inArray($state, [
            self::STATE_WAIT_FOR_SEND,
            self::STATE_SENT,
            self::STATE_CANCELED,
        ]);

        $this->state = $state;
    }
}
