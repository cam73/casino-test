<?php

namespace Framework\Database\Factory;

use Application\Component\Bank\BankTransaction;
use Application\Component\Bank\BankUserAccountFactory;
use Framework\Database\Resolver\BankTransactionStatusResolver;
use Framework\Database\Entity\ORMBankTransfer;

class BankTransactionFactory
{
    private MoneyPrizeFactory $moneyPrizeFactory;

    private BankUserAccountFactory $bankUserAccountFactory;

    public function __construct(
        MoneyPrizeFactory $moneyPrizeFactory,
        BankUserAccountFactory $bankUserAccountFactory
    )
    {
        $this->moneyPrizeFactory = $moneyPrizeFactory;
        $this->bankUserAccountFactory = $bankUserAccountFactory;
    }

    public function create(ORMBankTransfer $ORMBankTransfer): BankTransaction
    {
        return new BankTransaction(
            $ORMBankTransfer->getId(),
            $this->moneyPrizeFactory->create($ORMBankTransfer->getMoneyPrize()),
            $this->bankUserAccountFactory->create(
                $ORMBankTransfer->getPayeeBankTin(),
                $ORMBankTransfer->getPayeeAccountNumber()
            ),
            BankTransactionStatusResolver::resolveToDomain($ORMBankTransfer->getState()),
            $ORMBankTransfer->getAmount()
        );
    }
}
