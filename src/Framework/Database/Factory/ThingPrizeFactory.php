<?php

namespace Framework\Database\Factory;

use Application\Component\Prize\ThingPrize\ThingPrize;
use Framework\Database\Entity\ORMThingPrize;
use Framework\Database\Resolver\ThingPrizeStatusResolver;

class ThingPrizeFactory
{
    public function create(ORMThingPrize $ORMThingPrize): ThingPrize
    {
        return new ThingPrize(
            $ORMThingPrize->getId(),
            $ORMThingPrize->getThing()->getTitle(),
            ThingPrizeStatusResolver::resolveToDomain($ORMThingPrize->getStatus())
        );
    }
}
