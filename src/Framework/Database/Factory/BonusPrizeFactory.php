<?php

namespace Framework\Database\Factory;

use Application\Component\Prize\BonusPrize\BonusPrize;
use Application\Component\Prize\PrizeFactoryInterface;
use Framework\Database\Entity\ORMBonusPrize;
use Framework\Database\Resolver\BonusPrizeStatusResolver;

class BonusPrizeFactory
{
    public function create(ORMBonusPrize $ORMBonusPrize): BonusPrize
    {
        return new BonusPrize(
            $ORMBonusPrize->getId(),
            $ORMBonusPrize->getBonusTransaction()->getAmount(),
            BonusPrizeStatusResolver::resolveToDomain($ORMBonusPrize->getStatus())
        );
    }
}
