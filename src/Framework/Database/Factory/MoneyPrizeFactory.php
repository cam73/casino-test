<?php

namespace Framework\Database\Factory;

use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Prize\PrizeFactoryInterface;
use Framework\Database\Resolver\MoneyPrizeStatusResolver;
use Framework\Database\Entity\ORMMoneyPrize;

class MoneyPrizeFactory
{
    public function create(ORMMoneyPrize $ORMMoneyPrize): MoneyPrize
    {
        return new MoneyPrize(
            $ORMMoneyPrize->getId(),
            $ORMMoneyPrize->getMoneyTransaction()->getAmount(),
            MoneyPrizeStatusResolver::resolveToDomain($ORMMoneyPrize->getStatus())
        );
    }
}
