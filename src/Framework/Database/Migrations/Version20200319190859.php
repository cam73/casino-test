<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200319190859 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bonus_prize (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, bonus_transaction_id INT NOT NULL, status ENUM(\'available\', \'accepted\', \'declined\'), INDEX IDX_84700862A76ED395 (user_id), INDEX IDX_847008627E347B44 (bonus_transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bonus_transaction (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, amount NUMERIC(15, 2) NOT NULL, type ENUM(\'receipt\', \'expense\'), status ENUM(\'reserved\', \'actively\', \'canceled\'), INDEX IDX_487D3D7DA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE money_prize (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, money_transaction_id INT NOT NULL, status ENUM(\'available\', \'wait_for_transfer\', \'transferred\', \'converted_to_bonus\', \'declined\'), INDEX IDX_CF4392C1A76ED395 (user_id), INDEX IDX_CF4392C1E1492834 (money_transaction_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE money_transaction (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, own_of_fund TINYINT(1) DEFAULT \'0\' NOT NULL, amount NUMERIC(15, 2) NOT NULL, type ENUM(\'receipt\', \'expense\'), status ENUM(\'reserved\', \'actively\', \'transferred\', \'canceled\'), INDEX IDX_D21254E2A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thing (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, own_of_fund TINYINT(1) DEFAULT \'0\' NOT NULL, title VARCHAR(255) NOT NULL, status ENUM(\'reserved\', \'actively\', \'transferred\', \'canceled\'), INDEX IDX_5B4C2C83A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE thing_prize (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, thing_id INT NOT NULL, status ENUM(\'available\', \'transferred\', \'declined\'), INDEX IDX_F6CEE5FAA76ED395 (user_id), INDEX IDX_F6CEE5FAC36906A7 (thing_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, login VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bonus_prize ADD CONSTRAINT FK_84700862A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE bonus_prize ADD CONSTRAINT FK_847008627E347B44 FOREIGN KEY (bonus_transaction_id) REFERENCES bonus_transaction (id)');
        $this->addSql('ALTER TABLE bonus_transaction ADD CONSTRAINT FK_487D3D7DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE money_prize ADD CONSTRAINT FK_CF4392C1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE money_prize ADD CONSTRAINT FK_CF4392C1E1492834 FOREIGN KEY (money_transaction_id) REFERENCES money_transaction (id)');
        $this->addSql('ALTER TABLE money_transaction ADD CONSTRAINT FK_D21254E2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE thing ADD CONSTRAINT FK_5B4C2C83A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE thing_prize ADD CONSTRAINT FK_F6CEE5FAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE thing_prize ADD CONSTRAINT FK_F6CEE5FAC36906A7 FOREIGN KEY (thing_id) REFERENCES thing (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bonus_prize DROP FOREIGN KEY FK_847008627E347B44');
        $this->addSql('ALTER TABLE money_prize DROP FOREIGN KEY FK_CF4392C1E1492834');
        $this->addSql('ALTER TABLE thing_prize DROP FOREIGN KEY FK_F6CEE5FAC36906A7');
        $this->addSql('ALTER TABLE bonus_prize DROP FOREIGN KEY FK_84700862A76ED395');
        $this->addSql('ALTER TABLE bonus_transaction DROP FOREIGN KEY FK_487D3D7DA76ED395');
        $this->addSql('ALTER TABLE money_prize DROP FOREIGN KEY FK_CF4392C1A76ED395');
        $this->addSql('ALTER TABLE money_transaction DROP FOREIGN KEY FK_D21254E2A76ED395');
        $this->addSql('ALTER TABLE thing DROP FOREIGN KEY FK_5B4C2C83A76ED395');
        $this->addSql('ALTER TABLE thing_prize DROP FOREIGN KEY FK_F6CEE5FAA76ED395');
        $this->addSql('DROP TABLE bonus_prize');
        $this->addSql('DROP TABLE bonus_transaction');
        $this->addSql('DROP TABLE money_prize');
        $this->addSql('DROP TABLE money_transaction');
        $this->addSql('DROP TABLE thing');
        $this->addSql('DROP TABLE thing_prize');
        $this->addSql('DROP TABLE user');
    }
}
