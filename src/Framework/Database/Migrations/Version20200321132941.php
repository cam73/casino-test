<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200321132941 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bank_transfer (id INT AUTO_INCREMENT NOT NULL, money_prize_id INT NOT NULL, payee_bank_tin VARCHAR(255) NOT NULL, payee_account_number VARCHAR(255) NOT NULL, amount NUMERIC(15, 2) NOT NULL, state ENUM(\'wait_for_send\', \'sent\', \'canceled\'), UNIQUE INDEX UNIQ_7174B9475A8EC13A (money_prize_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_C7440455A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bank_transfer ADD CONSTRAINT FK_7174B9475A8EC13A FOREIGN KEY (money_prize_id) REFERENCES money_prize (id)');
        $this->addSql('ALTER TABLE client ADD CONSTRAINT FK_C7440455A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE bonus_prize DROP FOREIGN KEY FK_84700862A76ED395');
        $this->addSql('DROP INDEX IDX_84700862A76ED395 ON bonus_prize');
        $this->addSql('ALTER TABLE bonus_prize CHANGE status status ENUM(\'available\', \'accepted\', \'declined\'), CHANGE user_id client_id INT NOT NULL');
        $this->addSql('ALTER TABLE bonus_prize ADD CONSTRAINT FK_8470086219EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_8470086219EB6921 ON bonus_prize (client_id)');
        $this->addSql('ALTER TABLE bonus_transaction DROP FOREIGN KEY FK_487D3D7DA76ED395');
        $this->addSql('DROP INDEX IDX_487D3D7DA76ED395 ON bonus_transaction');
        $this->addSql('ALTER TABLE bonus_transaction CHANGE type type ENUM(\'receipt\', \'expense\'), CHANGE status status ENUM(\'reserved\', \'actively\', \'canceled\'), CHANGE user_id client_id INT NOT NULL');
        $this->addSql('ALTER TABLE bonus_transaction ADD CONSTRAINT FK_487D3D7D19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_487D3D7D19EB6921 ON bonus_transaction (client_id)');
        $this->addSql('ALTER TABLE money_prize DROP FOREIGN KEY FK_CF4392C1A76ED395');
        $this->addSql('DROP INDEX IDX_CF4392C1A76ED395 ON money_prize');
        $this->addSql('ALTER TABLE money_prize CHANGE status status ENUM(\'available\', \'wait_for_transfer\', \'transferred\', \'converted_to_bonus\', \'declined\'), CHANGE user_id client_id INT NOT NULL');
        $this->addSql('ALTER TABLE money_prize ADD CONSTRAINT FK_CF4392C119EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_CF4392C119EB6921 ON money_prize (client_id)');
        $this->addSql('ALTER TABLE money_transaction DROP FOREIGN KEY FK_D21254E2A76ED395');
        $this->addSql('DROP INDEX IDX_D21254E2A76ED395 ON money_transaction');
        $this->addSql('ALTER TABLE money_transaction CHANGE type type ENUM(\'receipt\', \'expense\'), CHANGE status status ENUM(\'reserved\', \'actively\', \'transferred\', \'canceled\'), CHANGE user_id client_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE money_transaction ADD CONSTRAINT FK_D21254E219EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_D21254E219EB6921 ON money_transaction (client_id)');
        $this->addSql('ALTER TABLE thing DROP FOREIGN KEY FK_5B4C2C83A76ED395');
        $this->addSql('DROP INDEX IDX_5B4C2C83A76ED395 ON thing');
        $this->addSql('ALTER TABLE thing CHANGE status status ENUM(\'reserved\', \'actively\', \'transferred\', \'canceled\'), CHANGE user_id client_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE thing ADD CONSTRAINT FK_5B4C2C8319EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_5B4C2C8319EB6921 ON thing (client_id)');
        $this->addSql('ALTER TABLE thing_prize DROP FOREIGN KEY FK_F6CEE5FAA76ED395');
        $this->addSql('DROP INDEX IDX_F6CEE5FAA76ED395 ON thing_prize');
        $this->addSql('ALTER TABLE thing_prize CHANGE status status ENUM(\'available\', \'transferred\', \'declined\'), CHANGE user_id client_id INT NOT NULL');
        $this->addSql('ALTER TABLE thing_prize ADD CONSTRAINT FK_F6CEE5FA19EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('CREATE INDEX IDX_F6CEE5FA19EB6921 ON thing_prize (client_id)');
        $this->addSql('ALTER TABLE user DROP name');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bonus_prize DROP FOREIGN KEY FK_8470086219EB6921');
        $this->addSql('ALTER TABLE bonus_transaction DROP FOREIGN KEY FK_487D3D7D19EB6921');
        $this->addSql('ALTER TABLE money_prize DROP FOREIGN KEY FK_CF4392C119EB6921');
        $this->addSql('ALTER TABLE money_transaction DROP FOREIGN KEY FK_D21254E219EB6921');
        $this->addSql('ALTER TABLE thing DROP FOREIGN KEY FK_5B4C2C8319EB6921');
        $this->addSql('ALTER TABLE thing_prize DROP FOREIGN KEY FK_F6CEE5FA19EB6921');
        $this->addSql('DROP TABLE bank_transfer');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP INDEX IDX_8470086219EB6921 ON bonus_prize');
        $this->addSql('ALTER TABLE bonus_prize CHANGE status status VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE client_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE bonus_prize ADD CONSTRAINT FK_84700862A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_84700862A76ED395 ON bonus_prize (user_id)');
        $this->addSql('DROP INDEX IDX_487D3D7D19EB6921 ON bonus_transaction');
        $this->addSql('ALTER TABLE bonus_transaction CHANGE type type VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE client_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE bonus_transaction ADD CONSTRAINT FK_487D3D7DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_487D3D7DA76ED395 ON bonus_transaction (user_id)');
        $this->addSql('DROP INDEX IDX_CF4392C119EB6921 ON money_prize');
        $this->addSql('ALTER TABLE money_prize CHANGE status status VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE client_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE money_prize ADD CONSTRAINT FK_CF4392C1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_CF4392C1A76ED395 ON money_prize (user_id)');
        $this->addSql('DROP INDEX IDX_D21254E219EB6921 ON money_transaction');
        $this->addSql('ALTER TABLE money_transaction CHANGE type type VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE status status VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE client_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE money_transaction ADD CONSTRAINT FK_D21254E2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_D21254E2A76ED395 ON money_transaction (user_id)');
        $this->addSql('DROP INDEX IDX_5B4C2C8319EB6921 ON thing');
        $this->addSql('ALTER TABLE thing CHANGE status status VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE client_id user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE thing ADD CONSTRAINT FK_5B4C2C83A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_5B4C2C83A76ED395 ON thing (user_id)');
        $this->addSql('DROP INDEX IDX_F6CEE5FA19EB6921 ON thing_prize');
        $this->addSql('ALTER TABLE thing_prize CHANGE status status VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE client_id user_id INT NOT NULL');
        $this->addSql('ALTER TABLE thing_prize ADD CONSTRAINT FK_F6CEE5FAA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_F6CEE5FAA76ED395 ON thing_prize (user_id)');
        $this->addSql('ALTER TABLE user ADD name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
