<?php

namespace Framework\Database\Storage;

use Application\Component\Bank\BankTransaction;
use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Storage\TransferMoneyPrizeStorageInterface;
use Assert\AssertionFailedException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Framework\Database\Factory\BankTransactionFactory;
use Framework\Database\Resolver\BankTransactionStatusResolver;
use Framework\Database\Resolver\MoneyPrizeStatusResolver;
use Framework\Database\Factory\MoneyPrizeFactory;
use Framework\Database\Resolver\MoneyTransactionStatusResolver;
use Framework\Database\Storage\Exception\DatabaseStorageException;
use Framework\Database\Entity\ORMMoneyPrize;
use Framework\Database\Entity\ORMBankTransfer;
use Framework\Database\Entity\ORMMoneyTransaction;
use Framework\Database\Repository\ORMBankTransferRepository;
use Framework\Database\Repository\ORMMoneyPrizeRepository;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class TransferMoneyPrizeStorage implements TransferMoneyPrizeStorageInterface
{
    private ORMMoneyPrizeRepository $ORMMoneyPrizeRepository;

    private ORMBankTransferRepository $ORMBankTransferRepository;

    private MoneyPrizeFactory $moneyPrizeFactory;

    private BankTransactionFactory $bankTransactionFactory;

    private EntityManagerInterface $em;

    private Security $security;

    public function __construct(
        ORMMoneyPrizeRepository $ORMMoneyPrizeRepository,
        ORMBankTransferRepository $ORMBankTransferRepository,
        MoneyPrizeFactory $moneyPrizeFactory,
        BankTransactionFactory $bankTransactionFactory,
        EntityManagerInterface $em,
        Security $security
    ) {
        $this->ORMMoneyPrizeRepository = $ORMMoneyPrizeRepository;
        $this->ORMBankTransferRepository = $ORMBankTransferRepository;
        $this->moneyPrizeFactory = $moneyPrizeFactory;
        $this->bankTransactionFactory = $bankTransactionFactory;
        $this->em = $em;
        $this->security = $security;
    }

    public function getMoneyPrize(string $moneyPrizeId): MoneyPrize
    {
        /** @var ORMMoneyPrize $ORMMoneyPrize */
        $ORMMoneyPrize = $this->ORMMoneyPrizeRepository->find($moneyPrizeId);
        $this->checkSecurity($ORMMoneyPrize);

        return $this->moneyPrizeFactory->create($ORMMoneyPrize);
    }

    /**
     * @param MoneyPrize $moneyPrize
     * @param BankTransaction $bankTransaction
     * @throws AssertionFailedException
     * @throws Exception
     */
    public function saveScheduleResult(MoneyPrize $moneyPrize, BankTransaction $bankTransaction): void
    {
        /** @var ORMMoneyPrize $ORMMoneyPrize */
        $ORMMoneyPrize = $this->ORMMoneyPrizeRepository->find($moneyPrize->getId());
        if (!$ORMMoneyPrize) {
            throw new DatabaseStorageException();
        }
        $this->checkSecurity($ORMMoneyPrize);

        $this->em->beginTransaction();
        try {
            $ORMMoneyPrize->setStatus(MoneyPrizeStatusResolver::resolveFromDomain($moneyPrize->getState()));

            $ORMBankTransfer = new ORMBankTransfer();
            $ORMBankTransfer->setAmount($bankTransaction->getAmount());
            $ORMBankTransfer->setState(BankTransactionStatusResolver::resolveFromDomain($bankTransaction->getStatus()));
            $ORMBankTransfer->setPayeeAccountNumber($bankTransaction->getBankUserAccount()->getAccountNumber());
            $ORMBankTransfer->setPayeeBankTin($bankTransaction->getBankUserAccount()->getBank()->getTin());
            $ORMBankTransfer->setMoneyPrize($ORMMoneyPrize);

            $this->em->persist($ORMMoneyPrize);
            $this->em->persist($ORMBankTransfer);
            $this->em->flush();
            $this->em->commit();

        } catch (Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    public function getBankTransaction(string $bankTransactionId): BankTransaction
    {
        /** @var ORMBankTransfer $ORMBankTransfer */
        $ORMBankTransfer = $this->ORMBankTransferRepository->find($bankTransactionId);

        return $this->bankTransactionFactory->create($ORMBankTransfer);
    }

    /**
     * @param BankTransaction $bankTransaction
     * @throws AssertionFailedException
     * @throws Exception
     */
    public function saveTransferResult(BankTransaction $bankTransaction): void
    {
        /** @var ORMBankTransfer $ORMBankTransfer */
        $ORMBankTransfer = $this->ORMBankTransferRepository->find($bankTransaction->getId());
        if (!$ORMBankTransfer) {
            throw new DatabaseStorageException();
        }

        $this->em->beginTransaction();
        try {

            $ORMBankTransfer
                ->setState(BankTransactionStatusResolver::resolveFromDomain(
                    $bankTransaction->getStatus()
                ));

            $ORMBankTransfer->getMoneyPrize()
                ->setStatus(MoneyPrizeStatusResolver::resolveFromDomain(
                    $bankTransaction->getMoneyPrize()->getState()
                ));

            $ORMBankTransfer->getMoneyPrize()->getMoneyTransaction()
                ->setStatus(MoneyTransactionStatusResolver::resolveFromDomain(
                    $bankTransaction->getStatus()
                ));

            $ORMFundExpenseMoneyTransaction = new ORMMoneyTransaction(
                null,
                true,
                $ORMBankTransfer->getMoneyPrize()->getMoneyTransaction()->getAmount(),
                ORMMoneyTransaction::TYPE_EXPENSE,
                ORMMoneyTransaction::STATUS_ACTIVELY
            );

            $this->em->persist($ORMFundExpenseMoneyTransaction);
            $this->em->persist($ORMBankTransfer);
            $this->em->flush();
            $this->em->commit();

        } catch (Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }


    private function checkSecurity(ORMMoneyPrize $ORMMoneyPrize)
    {
        if ($this->security->getUser() !== $ORMMoneyPrize->getClient()->getUser()) {
            throw new AccessDeniedException();
        }
    }
}
