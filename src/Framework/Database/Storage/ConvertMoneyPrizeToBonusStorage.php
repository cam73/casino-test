<?php

namespace Framework\Database\Storage;

use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Storage\ConvertMoneyPrizeToBonusStorageInterface;
use Assert\AssertionFailedException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Framework\Database\Entity\ORMBonusTransaction;
use Framework\Database\Entity\ORMMoneyPrize;
use Framework\Database\Entity\ORMMoneyTransaction;
use Framework\Database\Factory\MoneyPrizeFactory;
use Framework\Database\Repository\ORMMoneyPrizeRepository;
use Framework\Database\Resolver\MoneyPrizeStatusResolver;
use Framework\Database\Storage\Exception\DatabaseStorageException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class ConvertMoneyPrizeToBonusStorage implements ConvertMoneyPrizeToBonusStorageInterface
{
    private ORMMoneyPrizeRepository $ORMMoneyPrizeRepository;

    private MoneyPrizeFactory $moneyPrizeFactory;

    private EntityManagerInterface $em;

    private Security $security;

    public function __construct(
        ORMMoneyPrizeRepository $ORMMoneyPrizeRepository,
        MoneyPrizeFactory $moneyPrizeFactory,
        EntityManagerInterface $em,
        Security $security
    ) {
        $this->ORMMoneyPrizeRepository = $ORMMoneyPrizeRepository;
        $this->moneyPrizeFactory = $moneyPrizeFactory;
        $this->em = $em;
        $this->security = $security;
    }

    public function findMoneyPrize(string $moneyPrizeId): MoneyPrize
    {
        /** @var ORMMoneyPrize $ORMMoneyPrize */
        $ORMMoneyPrize = $this->ORMMoneyPrizeRepository->find($moneyPrizeId);
        $this->checkSecurity($ORMMoneyPrize);

        return $this->moneyPrizeFactory->create($ORMMoneyPrize);
    }

    /**
     * @param MoneyPrize $moneyPrize
     * @param string $bonusAmount
     * @throws AssertionFailedException
     * @throws Exception
     */
    public function saveConvertMoneyPrizeToBonusResult(MoneyPrize $moneyPrize, string $bonusAmount)
    {
        /** @var ORMMoneyPrize $ORMMoneyPrize */
        $ORMMoneyPrize = $this->ORMMoneyPrizeRepository->find($moneyPrize->getId());
        if (!$ORMMoneyPrize) {
            throw new DatabaseStorageException();
        }
        $this->checkSecurity($ORMMoneyPrize);

        $this->em->beginTransaction();
        try {
            $ORMMoneyPrize->setStatus(MoneyPrizeStatusResolver::resolveFromDomain($moneyPrize->getState()));

            $ORMMoneyPrize->getMoneyTransaction()->setStatus(ORMMoneyTransaction::STATUS_ACTIVELY);

            $ORMBonusTransaction = new ORMBonusTransaction(
                $ORMMoneyPrize->getClient(),
                $bonusAmount,
                ORMBonusTransaction::TYPE_RECEIPT,
                ORMBonusTransaction::STATUS_ACTIVELY
            );

            $this->em->persist($ORMMoneyPrize);
            $this->em->persist($ORMBonusTransaction);
            $this->em->flush();
            $this->em->commit();

        } catch (Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }

    private function checkSecurity(ORMMoneyPrize $ORMMoneyPrize)
    {
        if ($this->security->getUser() !== $ORMMoneyPrize->getClient()->getUser()) {
            throw new AccessDeniedException();
        }
    }
}
