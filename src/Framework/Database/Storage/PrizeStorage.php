<?php

namespace Framework\Database\Storage;

use Application\Component\Prize\BonusPrize\BonusPrize;
use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Prize\PrizeInterface;
use Application\Component\Prize\ThingPrize\ThingPrize;
use Application\Component\Storage\PrizeStorageInterface;
use Assert\AssertionFailedException;
use Exception;
use Framework\Database\Entity\ORMBonusPrize;
use Framework\Database\Entity\ORMMoneyPrize;
use Framework\Database\Entity\ORMThingPrize;
use Framework\Database\Repository\ORMClientRepository;
use Framework\Database\Storage\PrizeProvider\BonusPrizeProvider;
use Framework\Database\Storage\PrizeProvider\PrizeProviderResolver;
use Framework\Database\Storage\PrizeProvider\ThingPrizeProvider;

class PrizeStorage implements PrizeStorageInterface
{
    private PrizeProviderResolver $prizeProviderResolver;

    private ORMClientRepository $ORMClientRepository;

    public function __construct(
        PrizeProviderResolver $prizeProviderResolver,
        ORMClientRepository $ORMClientRepository
    )
    {
        $this->prizeProviderResolver = $prizeProviderResolver;
        $this->ORMClientRepository = $ORMClientRepository;
    }

    /**
     * @param string $clientId
     * @return BonusPrize[]
     */
    public function getClientBonusPrizes(string $clientId): array
    {
        return $this->prizeProviderResolver->resolveByClass(BonusPrize::class)
            ->getClientPrizes($clientId);
    }

    /**
     * @param string $clientId
     * @return MoneyPrize[]
     */
    public function getClientMoneyPrizes(string $clientId): array
    {
        return $this->prizeProviderResolver->resolveByClass(MoneyPrize::class)
            ->getClientPrizes($clientId);
    }

    /**
     * @param string $clientId
     * @return ThingPrize[]
     */
    public function getClientThingPrizes(string $clientId): array
    {
        return $this->prizeProviderResolver->resolveByClass(ThingPrize::class)
            ->getClientPrizes($clientId);
    }

    /**
     * @param string $clientId
     * @return PrizeInterface
     * @throws AssertionFailedException
     * @throws Exception
     */
    public function giveRandomPrize(string $clientId): PrizeInterface
    {
        $randomProvider = $this->prizeProviderResolver->getRandomProviderForGivePrize();
        /** @var ORMBonusPrize|ORMMoneyPrize|ORMThingPrize $ORMPrize */
        return $randomProvider->giveRandomPrizeToClient($clientId);
    }

    public function findPrize(string $prizeClass, string $prizeId): PrizeInterface
    {
        return $this->prizeProviderResolver->resolveByClass($prizeClass)->getPrize($prizeId);
    }

    public function saveDeclineResult(PrizeInterface $prize)
    {
        $this->prizeProviderResolver->resolveByClass(get_class($prize))->saveDeclineResult($prize);
    }

    public function saveRefillBonusResult(BonusPrize $bonusPrize)
    {
        /** @var BonusPrizeProvider $bonusPrizeProvider */
        $bonusPrizeProvider = $this->prizeProviderResolver->resolveByClass(BonusPrize::class);

        $bonusPrizeProvider->saveRefillBonusResult($bonusPrize);
    }

    public function saveSendThingPrizeResult(ThingPrize $thingPrize)
    {
        /** @var ThingPrizeProvider $thingPrizeProvider */
        $thingPrizeProvider = $this->prizeProviderResolver->resolveByClass(ThingPrize::class);

        $thingPrizeProvider->saveSendThingPrizeResult($thingPrize);
    }
}
