<?php

namespace Framework\Database\Storage;

use Application\Component\Storage\FundStorageInterface;
use Application\Component\Thing\Thing;
use Framework\Database\Repository\ORMMoneyRepository;
use Framework\Database\Repository\ORMThingRepository;

class FundStorage implements FundStorageInterface
{
    private ORMThingRepository $ORMThingRepository;

    private ORMMoneyRepository $ORMMoneyRepository;

    public function __construct(ORMThingRepository $ORMThingRepository, ORMMoneyRepository $ORMMoneyRepository)
    {
        $this->ORMThingRepository = $ORMThingRepository;
        $this->ORMMoneyRepository = $ORMMoneyRepository;
    }

    public function getFundMoneyBalanceAmount(): string
    {
        return $this->ORMMoneyRepository->getFundMoneyBalanceAmount();
    }

    /**
     * @return Thing[]
     */
    public function getFundThings(): array
    {
        $ORMFundThings = $this->ORMThingRepository->findFundThings();

        $things = [];
        foreach ($ORMFundThings as $ORMFundThing) {
            $things[] = new Thing($ORMFundThing->getTitle());
        }

        return $things;
    }
}
