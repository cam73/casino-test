<?php

namespace Framework\Database\Storage;

use Application\Component\Storage\BonusStorageInterface;
use Framework\Database\Entity\ORMClient;
use Framework\Database\Repository\ORMBonusRepository;
use Framework\Database\Repository\ORMClientRepository;

class BonusStorage implements BonusStorageInterface
{
    private ORMBonusRepository $ORMBonusRepository;

    private ORMClientRepository $ORMClientRepository;

    public function __construct(
        ORMBonusRepository $ORMBonusRepository,
        ORMClientRepository $ORMClientRepository
    )
    {
        $this->ORMBonusRepository = $ORMBonusRepository;
        $this->ORMClientRepository = $ORMClientRepository;
    }

    public function getBonusBalanceAmountByClientId(string $ORMClientId): string
    {
        /** @var ORMClient $ORMClient */
        $ORMClient = $this->ORMClientRepository->find($ORMClientId);

        return $this->ORMBonusRepository->getBonusBalanceAmount($ORMClient);
    }
}
