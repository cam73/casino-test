<?php

namespace Framework\Database\Storage\PrizeProvider;

use Application\Component\Prize\PrizeInterface;
use Application\Component\Prize\ThingPrize\ThingPrize;
use Application\Component\Prize\ThingPrize\ThingPrizeProviderInterface;
use Assert\AssertionFailedException;
use Doctrine\ORM\EntityManagerInterface;
use Framework\Database\Entity\ORMClient;
use Framework\Database\Entity\ORMThing;
use Framework\Database\Entity\ORMThingPrize;
use Framework\Database\Factory\ThingPrizeFactory;
use Framework\Database\Repository\ORMClientRepository;
use Framework\Database\Repository\ORMThingPrizeRepository;
use Framework\Database\Repository\ORMThingRepository;
use Framework\Database\Resolver\ThingPrizeStatusResolver;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class ThingPrizeProvider implements ThingPrizeProviderInterface
{
    private ORMThingPrizeRepository $ORMThingPrizeRepository;

    private ORMThingRepository $ORMThingRepository;

    private ORMClientRepository $ORMClientRepository;

    private EntityManagerInterface $em;

    private ThingPrizeFactory $thingPrizeFactory;

    private Security $security;

    public function __construct(
        ORMThingPrizeRepository $ORMThingPrizeRepository,
        ORMThingRepository $ORMThingRepository,
        ORMClientRepository $ORMClientRepository,
        EntityManagerInterface $em,
        ThingPrizeFactory $thingPrizeFactory,
        Security $security
    ) {
        $this->ORMThingPrizeRepository = $ORMThingPrizeRepository;
        $this->ORMThingRepository = $ORMThingRepository;
        $this->ORMClientRepository = $ORMClientRepository;
        $this->em = $em;
        $this->thingPrizeFactory = $thingPrizeFactory;
        $this->security = $security;
    }

    public function isSupportClass(string $className): bool
    {
        return in_array($className, [ThingPrize::class]);
    }

    /**
     * @param string $clientId
     * @return array|ThingPrize[]
     */
    public function getClientPrizes(string $clientId): array
    {
        /** @var ORMClient $client */
        $client = $this->ORMClientRepository->find($clientId);

        /** @var ORMThingPrize[] $thingPrizes */
        $thingPrizes = $this->ORMThingPrizeRepository->findThingPrizes($client);

        $domainPrizes = [];
        foreach ($thingPrizes as $thingPrize) {
            $domainPrizes[] = $this->thingPrizeFactory->create($thingPrize);
        }

        return $domainPrizes;
    }

    /**
     * @param string $clientId
     * @return PrizeInterface|ThingPrize
     */
    public function giveRandomPrizeToClient(string $clientId): ThingPrize
    {
        /** @var ORMClient $ORMClient */
        $ORMClient = $this->ORMClientRepository->find($clientId);

        $ORMThingPrize = $this->ORMThingPrizeRepository->registryPrize(
            $ORMClient,
            $this->ORMThingRepository->getRandomThing()
        );

        $this->em->persist($ORMThingPrize);
        $this->em->flush();

        return $this->thingPrizeFactory->create($ORMThingPrize);
    }

    public function canGivePrize(): bool
    {
        return ($this->ORMThingRepository->getFundThingsAmount() > 0);
    }

    public function getPrize(string $prizeId): PrizeInterface
    {
        /** @var ORMThingPrize $ORMThingPrize */
        $ORMThingPrize = $this->ORMThingPrizeRepository->find($prizeId);
        $this->checkSecurity($ORMThingPrize);

        return $this->thingPrizeFactory->create($ORMThingPrize);
    }

    /**
     * @param PrizeInterface|ThingPrize $prize
     * @throws AssertionFailedException
     */
    public function saveDeclineResult(PrizeInterface $prize)
    {
        /** @var ORMThingPrize $ORMThingPrize */
        $ORMThingPrize = $this->ORMThingPrizeRepository->find($prize->getId());
        $this->checkSecurity($ORMThingPrize);

        $ORMThingPrize->getThing()->setClient(null);
        $ORMThingPrize->getThing()->setStatus(ORMThing::STATUS_ACTIVELY);
        $ORMThingPrize->setStatus(ThingPrizeStatusResolver::resolveFromDomain($prize->getState()));

        $this->em->persist($ORMThingPrize);
        $this->em->flush();
    }

    public function saveSendThingPrizeResult(ThingPrize $thingPrize)
    {
        /** @var ORMThingPrize $ORMThingPrize */
        $ORMThingPrize = $this->ORMThingPrizeRepository->find($thingPrize->getId());
        $this->checkSecurity($ORMThingPrize);

        $ORMThingPrize->getThing()->setStatus(ORMThing::STATUS_TRANSFERRED);
        $ORMThingPrize->setStatus(ThingPrizeStatusResolver::resolveFromDomain($thingPrize->getState()));

        $this->em->persist($ORMThingPrize);
        $this->em->flush();
    }

    private function checkSecurity(ORMThingPrize $ORMThingPrize)
    {
        if ($this->security->getUser() !== $ORMThingPrize->getClient()->getUser()) {
            throw new AccessDeniedException();
        }
    }
}
