<?php

namespace Framework\Database\Storage\PrizeProvider;

use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Prize\MoneyPrize\MoneyPrizeProviderInterface;
use Application\Component\Prize\PrizeInterface;
use Assert\AssertionFailedException;
use Doctrine\ORM\EntityManagerInterface;
use Framework\Database\Entity\ORMClient;
use Framework\Database\Entity\ORMMoneyPrize;
use Framework\Database\Entity\ORMMoneyTransaction;
use Framework\Database\Factory\MoneyPrizeFactory;
use Framework\Database\Repository\ORMBankTransferRepository;
use Framework\Database\Repository\ORMClientRepository;
use Framework\Database\Repository\ORMMoneyPrizeRepository;
use Framework\Database\Repository\ORMMoneyRepository;
use Framework\Database\Resolver\MoneyPrizeStatusResolver;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class MoneyPrizeProvider implements MoneyPrizeProviderInterface
{
    private ORMMoneyPrizeRepository $ORMMoneyPrizeRepository;

    private ORMBankTransferRepository $ORMBankTransferRepository;

    private ORMMoneyRepository $ORMMoneyRepository;

    private ORMClientRepository $ORMClientRepository;

    private EntityManagerInterface $em;

    private MoneyPrizeFactory $moneyPrizeFactory;

    private Security $security;

    public function __construct(
        ORMMoneyPrizeRepository $ORMMoneyPrizeRepository,
        ORMBankTransferRepository $ORMBankTransferRepository,
        ORMMoneyRepository $ORMMoneyRepository,
        ORMClientRepository $ORMClientRepository,
        EntityManagerInterface $em,
        MoneyPrizeFactory $moneyPrizeFactory,
        Security $security
    )
    {
        $this->ORMMoneyPrizeRepository = $ORMMoneyPrizeRepository;
        $this->ORMBankTransferRepository = $ORMBankTransferRepository;
        $this->ORMMoneyRepository = $ORMMoneyRepository;
        $this->ORMClientRepository = $ORMClientRepository;
        $this->em = $em;
        $this->moneyPrizeFactory = $moneyPrizeFactory;
        $this->security = $security;
    }

    public function isSupportClass(string $className): bool
    {
        return in_array($className, [MoneyPrize::class]);
    }


    /**
     * @param string $clientId
     * @return MoneyPrize[]
     */
    public function getClientPrizes(string $clientId): array
    {
        /** @var ORMClient $ORMClient */
        $ORMClient = $this->ORMClientRepository->find($clientId);

        $ORMMoneyPrizes = $this->ORMMoneyPrizeRepository->findMoneyPrizes($ORMClient);

        $domainPrizes = [];
        foreach ($ORMMoneyPrizes as $ORMMoneyPrize) {
            $domainPrizes[] = $this->moneyPrizeFactory->create($ORMMoneyPrize);
        }

        return $domainPrizes;
    }

    /**
     * @param string $clientId
     * @return PrizeInterface|MoneyPrize
     * @throws AssertionFailedException
     */
    public function giveRandomPrizeToClient(string $clientId): MoneyPrize
    {
        /** @var ORMClient $ORMClient */
        $ORMClient = $this->ORMClientRepository->find($clientId);

        $ORMMoneyPrize = $this->ORMMoneyPrizeRepository->registryRandomPrize(
            $ORMClient,
            $this->ORMMoneyRepository->getFundMoneyBalanceAmount()
        );

        $this->em->persist($ORMMoneyPrize);
        $this->em->flush();

        return $this->moneyPrizeFactory->create($ORMMoneyPrize);
    }

    public function canGivePrize(): bool
    {
        return ($this->ORMMoneyRepository->getFundMoneyBalanceAmount() > 1);
    }

    /**
     * @param PrizeInterface|MoneyPrize $prize
     * @throws AssertionFailedException
     */
    public function saveDeclineResult(PrizeInterface $prize)
    {
        /** @var ORMMoneyPrize $ORMMoneyPrize */
        $ORMMoneyPrize = $this->ORMMoneyPrizeRepository->find($prize->getId());
        $this->checkSecurity($ORMMoneyPrize);

        $ORMMoneyPrize->getMoneyTransaction()->setStatus(ORMMoneyTransaction::STATUS_CANCELED);
        $ORMMoneyPrize->setStatus(MoneyPrizeStatusResolver::resolveFromDomain($prize->getState()));

        $this->em->persist($ORMMoneyPrize);
        $this->em->flush();
    }

    public function getPrize(string $prizeId): PrizeInterface
    {
        /** @var ORMMoneyPrize $ORMMoneyPrize */
        $ORMMoneyPrize = $this->ORMMoneyPrizeRepository->find($prizeId);
        $this->checkSecurity($ORMMoneyPrize);

        return $this->moneyPrizeFactory->create($ORMMoneyPrize);
    }

    private function checkSecurity(ORMMoneyPrize $ORMMoneyPrize)
    {
        if ($this->security->getUser() !== $ORMMoneyPrize->getClient()->getUser()) {
            throw new AccessDeniedException();
        }
    }

}
