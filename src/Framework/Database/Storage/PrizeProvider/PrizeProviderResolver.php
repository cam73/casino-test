<?php

namespace Framework\Database\Storage\PrizeProvider;

use Application\Component\Prize\PrizeProviderInterface;
use Application\Component\Prize\PrizeProviderResolverInterface;
use Assert\Assertion;
use Assert\AssertionFailedException;

class PrizeProviderResolver implements PrizeProviderResolverInterface
{
    /** @var PrizeProviderInterface[]  */
    private array $concretePrizeProviders;

    /**
     * @param PrizeProviderInterface[] $concretePrizeProviders
     */
    public function __construct(array $concretePrizeProviders)
    {
        $this->concretePrizeProviders = $concretePrizeProviders;
    }

    public function resolveByClass(string $className): PrizeProviderInterface
    {
        foreach ($this->concretePrizeProviders as $prizeProvider)
        {
            if ($prizeProvider->isSupportClass($className)) {
                return $prizeProvider;
            }
        }
    }

    /**
     * @return PrizeProviderInterface
     * @throws AssertionFailedException
     */
    public function getRandomProviderForGivePrize(): PrizeProviderInterface
    {
        $filledSourcePrizes = $this->getAvailableProvidersForGivePrize();
        Assertion::greaterThan(count($filledSourcePrizes), 0);

        return $filledSourcePrizes[array_rand($filledSourcePrizes)];
    }

    /**
     * @return PrizeProviderInterface[]
     */
    private function getAvailableProvidersForGivePrize(): array
    {
        return array_filter($this->concretePrizeProviders, function (PrizeProviderInterface $prizeProvider) {
            return $prizeProvider->canGivePrize();
        });
    }
}
