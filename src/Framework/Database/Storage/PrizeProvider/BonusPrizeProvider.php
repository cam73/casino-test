<?php

namespace Framework\Database\Storage\PrizeProvider;

use Application\Component\Prize\BonusPrize\BonusPrize;
use Application\Component\Prize\BonusPrize\BonusPrizeProviderInterface;
use Application\Component\Prize\PrizeInterface;
use Assert\AssertionFailedException;
use Doctrine\ORM\EntityManagerInterface;
use Framework\Database\Entity\ORMBonusPrize;
use Framework\Database\Entity\ORMBonusTransaction;
use Framework\Database\Entity\ORMClient;
use Framework\Database\Factory\BonusPrizeFactory;
use Framework\Database\Repository\ORMBonusPrizeRepository;
use Framework\Database\Repository\ORMClientRepository;
use Framework\Database\Resolver\BonusPrizeStatusResolver;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Security;

class BonusPrizeProvider implements BonusPrizeProviderInterface
{
    private ORMBonusPrizeRepository $ORMBonusPrizeRepository;

    private ORMClientRepository $ORMClientRepository;

    private BonusPrizeFactory $bonusPrizeFactory;

    private EntityManagerInterface $em;

    private Security $security;

    private int $maxSizeOfBonusPrize;

    public function __construct(
        string $maxSizeOfBonusPrize,
        ORMBonusPrizeRepository $ORMBonusPrizeRepository,
        ORMClientRepository $ORMClientRepository,
        BonusPrizeFactory $bonusPrizeFactory,
        EntityManagerInterface $em,
        Security $security
    )
    {
        $this->maxSizeOfBonusPrize = $maxSizeOfBonusPrize;
        $this->ORMBonusPrizeRepository = $ORMBonusPrizeRepository;
        $this->bonusPrizeFactory = $bonusPrizeFactory;
        $this->ORMClientRepository = $ORMClientRepository;
        $this->em = $em;
        $this->security = $security;
    }

    public function isSupportClass(string $className): bool
    {
        return in_array($className, [BonusPrize::class]);
    }

    /**
     * @param string $clientId
     * @return BonusPrize[]
     */
    public function getClientPrizes(string $clientId): array
    {
        /** @var ORMClient $ORMClient */
        $ORMClient = $this->ORMClientRepository->find($clientId);

        $ORMBonusPrizes = $this->ORMBonusPrizeRepository->findBonusPrizes($ORMClient);

        $domainPrizes = [];
        foreach ($ORMBonusPrizes as $ORMBonusPrize) {
            $domainPrizes[] = $this->bonusPrizeFactory
                ->create($ORMBonusPrize);
        }

        return $domainPrizes;
    }

    /**
     * @param string $clientId
     * @return PrizeInterface|BonusPrize
     */
    public function giveRandomPrizeToClient(string $clientId): BonusPrize
    {
        /** @var ORMClient $ORMClient */
        $ORMClient = $this->ORMClientRepository->find($clientId);

        $ORMBonusPrize = $this->ORMBonusPrizeRepository->registryRandomPrize(
            $ORMClient,
            $this->maxSizeOfBonusPrize
        );

        $this->em->persist($ORMBonusPrize);
        $this->em->flush();

        return $this->bonusPrizeFactory->create($ORMBonusPrize);
    }

    public function canGivePrize(): bool
    {
        return true;
    }

    /**
     * @param string $prizeId
     * @return bool
     */
    public function declinePrize(string $prizeId): bool
    {
        /** @var ORMBonusPrize $ORMBonusPrize */
        $ORMBonusPrize = $this->ORMBonusPrizeRepository->find($prizeId);
        $this->checkSecurity($ORMBonusPrize);

        $this->ORMBonusPrizeRepository->declinePrize($ORMBonusPrize);

        return true;
    }

    /**
     * @param PrizeInterface|BonusPrize $prize
     * @throws AssertionFailedException
     */
    public function saveDeclineResult(PrizeInterface $prize)
    {
        /** @var ORMBonusPrize $ORMBonusPrize */
        $ORMBonusPrize = $this->ORMBonusPrizeRepository->find($prize->getId());
        $this->checkSecurity($ORMBonusPrize);

        $ORMBonusPrize->getBonusTransaction()->setStatus(ORMBonusTransaction::STATUS_CANCELED);
        $ORMBonusPrize->setStatus(BonusPrizeStatusResolver::resolveFromDomain($prize->getState()));

        $this->em->persist($ORMBonusPrize);
        $this->em->flush();
    }

    public function saveRefillBonusResult(BonusPrize $bonusPrize)
    {
        /** @var ORMBonusPrize $ORMBonusPrize */
        $ORMBonusPrize = $this->ORMBonusPrizeRepository->find($bonusPrize->getId());
        $this->checkSecurity($ORMBonusPrize);

        $ORMBonusPrize->getBonusTransaction()->setStatus(ORMBonusTransaction::STATUS_ACTIVELY);
        $ORMBonusPrize->setStatus(BonusPrizeStatusResolver::resolveFromDomain($bonusPrize->getState()));

        $this->em->persist($ORMBonusPrize);
        $this->em->flush();
    }

    public function getPrize(string $prizeId): PrizeInterface
    {
        /** @var ORMBonusPrize $ORMBonusPrize */
        $ORMBonusPrize = $this->ORMBonusPrizeRepository->find($prizeId);
        $this->checkSecurity($ORMBonusPrize);

        return $this->bonusPrizeFactory->create($ORMBonusPrize);
    }

    private function checkSecurity(ORMBonusPrize $ORMBonusPrize)
    {
        if ($this->security->getUser() !== $ORMBonusPrize->getClient()->getUser()) {
            throw new AccessDeniedException();
        }
    }
}
