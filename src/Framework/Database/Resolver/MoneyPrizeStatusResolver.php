<?php

namespace Framework\Database\Resolver;

use Application\Component\Prize\MoneyPrize\MoneyPrizeState;
use Framework\Database\Entity\ORMMoneyPrize;

class MoneyPrizeStatusResolver
{
    public static function resolveFromDomain(MoneyPrizeState $domainStatus): string
    {
        switch ($domainStatus->getValue())
        {
            case MoneyPrizeState::AVAILABLE()->getValue():
                return ORMMoneyPrize::STATUS_AVAILABLE;
            case MoneyPrizeState::WAIT_FOR_TRANSFER()->getValue():
                return ORMMoneyPrize::STATUS_WAIT_FOR_TRANSFER;
            case MoneyPrizeState::TRANSFERRED()->getValue():
                return ORMMoneyPrize::STATUS_TRANSFERRED;
            case MoneyPrizeState::CONVERTED_TO_BONUS()->getValue():
                return ORMMoneyPrize::STATUS_CONVERTED_TO_BONUS;
            case MoneyPrizeState::DECLINED()->getValue():
                return ORMMoneyPrize::STATUS_DECLINED;
        }
    }

    public static function resolveToDomain(string $status): MoneyPrizeState
    {
        switch ($status) {
            case ORMMoneyPrize::STATUS_AVAILABLE:
                return MoneyPrizeState::AVAILABLE();
            case ORMMoneyPrize::STATUS_WAIT_FOR_TRANSFER:
                return MoneyPrizeState::WAIT_FOR_TRANSFER();
            case ORMMoneyPrize::STATUS_TRANSFERRED:
                return MoneyPrizeState::TRANSFERRED();
            case ORMMoneyPrize::STATUS_CONVERTED_TO_BONUS:
                return MoneyPrizeState::CONVERTED_TO_BONUS();
            case ORMMoneyPrize::STATUS_DECLINED:
                return MoneyPrizeState::DECLINED();
        }
    }
}
