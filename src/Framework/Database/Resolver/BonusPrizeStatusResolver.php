<?php

namespace Framework\Database\Resolver;

use Application\Component\Prize\BonusPrize\BonusPrizeState;
use Framework\Database\Entity\ORMBonusPrize;

class BonusPrizeStatusResolver
{
    public static function resolveFromDomain(BonusPrizeState $domainStatus): string
    {
        switch ($domainStatus->getValue())
        {
            case BonusPrizeState::AVAILABLE()->getValue():
                return ORMBonusPrize::STATUS_AVAILABLE;
            case BonusPrizeState::ACCEPTED()->getValue():
                return ORMBonusPrize::STATUS_ACCEPTED;
            case BonusPrizeState::DECLINED()->getValue():
                return ORMBonusPrize::STATUS_DECLINED;
        }
    }

    public static function resolveToDomain(string $status): BonusPrizeState
    {
        switch ($status) {
            case ORMBonusPrize::STATUS_AVAILABLE:
                return BonusPrizeState::AVAILABLE();
            case ORMBonusPrize::STATUS_ACCEPTED:
                return BonusPrizeState::ACCEPTED();
            case ORMBonusPrize::STATUS_DECLINED:
                return BonusPrizeState::DECLINED();
        }
    }
}
