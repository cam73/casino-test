<?php

namespace Framework\Database\Resolver;

use Application\Component\Prize\ThingPrize\ThingPrizeState;
use Framework\Database\Entity\ORMThingPrize;

class ThingPrizeStatusResolver
{
    public static function resolveFromDomain(ThingPrizeState $domainStatus): string
    {
        switch ($domainStatus->getValue())
        {
            case ThingPrizeState::AVAILABLE()->getValue():
                return ORMThingPrize::STATUS_AVAILABLE;
            case ThingPrizeState::TRANSFERRED()->getValue():
                return ORMThingPrize::STATUS_TRANSFERRED;
            case ThingPrizeState::DECLINED()->getValue():
                return ORMThingPrize::STATUS_DECLINED;
        }
    }

    public static function resolveToDomain(string $status): ThingPrizeState
    {
        switch ($status) {
            case ORMThingPrize::STATUS_AVAILABLE:
                return ThingPrizeState::AVAILABLE();
            case ORMThingPrize::STATUS_TRANSFERRED:
                return ThingPrizeState::TRANSFERRED();
            case ORMThingPrize::STATUS_DECLINED:
                return ThingPrizeState::DECLINED();
        }
    }
}
