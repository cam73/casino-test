<?php

namespace Framework\Database\Resolver;

use Application\Component\Bank\BankTransactionStatus;
use Framework\Database\Entity\ORMBankTransfer;

class BankTransactionStatusResolver
{
    public static function resolveFromDomain(BankTransactionStatus $domainStatus): string
    {
        switch ($domainStatus->getValue())
        {
            case BankTransactionStatus::PENDING()->getValue():
                return ORMBankTransfer::STATE_WAIT_FOR_SEND;
            case BankTransactionStatus::SENT()->getValue():
                return ORMBankTransfer::STATE_SENT;
            case BankTransactionStatus::CANCELED()->getValue():
                return ORMBankTransfer::STATE_CANCELED;
        }
    }

    public static function resolveToDomain(string $status): BankTransactionStatus
    {
        switch ($status) {
            case ORMBankTransfer::STATE_WAIT_FOR_SEND:
                return BankTransactionStatus::PENDING();
            case ORMBankTransfer::STATE_SENT:
                return BankTransactionStatus::SENT();
            case ORMBankTransfer::STATE_CANCELED:
                return BankTransactionStatus::CANCELED();
        }
    }
}
