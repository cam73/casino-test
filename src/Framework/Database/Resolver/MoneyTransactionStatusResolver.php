<?php

namespace Framework\Database\Resolver;

use Application\Component\Bank\BankTransactionStatus;
use Framework\Database\Entity\ORMMoneyTransaction;

class MoneyTransactionStatusResolver
{
    public static function resolveFromDomain(BankTransactionStatus $bankTransactionStatus): string
    {
        switch ($bankTransactionStatus->getValue())
        {
            case BankTransactionStatus::PENDING()->getValue():
                return ORMMoneyTransaction::STATUS_ACTIVELY;
            case BankTransactionStatus::SENT()->getValue():
                return ORMMoneyTransaction::STATUS_TRANSFERRED;
            case BankTransactionStatus::CANCELED()->getValue():
                return ORMMoneyTransaction::STATUS_CANCELED;
        }
    }

    public static function resolveToDomain(string $status): BankTransactionStatus
    {
        switch ($status) {
            case ORMMoneyTransaction::STATUS_ACTIVELY:
                return BankTransactionStatus::PENDING();
            case ORMMoneyTransaction::STATUS_TRANSFERRED:
                return BankTransactionStatus::SENT();
            case ORMMoneyTransaction::STATUS_CANCELED:
                return BankTransactionStatus::CANCELED();
        }
    }
}
