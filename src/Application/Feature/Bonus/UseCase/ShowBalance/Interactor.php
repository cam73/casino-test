<?php

namespace Application\Feature\Bonus\UseCase\ShowBalance;

use Application\Component\Storage\BonusStorageInterface;
use Application\Feature\Bonus\UseCase\ShowBalance\View\BalanceView;

class Interactor
{
    private BonusStorageInterface $bonusStorage;

    public function __construct(BonusStorageInterface $bonusStorage)
    {
        $this->bonusStorage = $bonusStorage;
    }

    public function getView(string $clientId): BalanceView
    {
        return new BalanceView($this->bonusStorage->getBonusBalanceAmountByClientId($clientId));
    }
}
