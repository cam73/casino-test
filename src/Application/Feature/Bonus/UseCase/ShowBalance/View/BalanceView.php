<?php

namespace Application\Feature\Bonus\UseCase\ShowBalance\View;

class BalanceView
{
    private string $amount;

    public function __construct(string $amount)
    {
        $this->amount = $amount;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }
}
