<?php

namespace Application\Feature\Prize\UseCase\ShowList\View;

class PrizeListView
{
    /** @var PrizeView[]|array */
    private array $prizes;

    /**
     * PrizeListView constructor.
     * @param PrizeView[]|array $prizes
     */
    public function __construct(array $prizes)
    {
        $this->prizes = $prizes;
    }

    /**
     * @return PrizeView[]|array
     */
    public function getPrizes(): array
    {
        return $this->prizes;
    }
}
