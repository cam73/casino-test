<?php

namespace Application\Feature\Prize\UseCase\ShowList\View;

use Application\Component\Prize\BonusPrize\BonusPrize;
use Application\Component\Prize\BonusPrize\BonusPrizeState;
use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Prize\MoneyPrize\MoneyPrizeState;
use Application\Component\Prize\ThingPrize\ThingPrize;
use Application\Component\Prize\ThingPrize\ThingPrizeState;

class PrizeViewFactory
{
    public function createByBonusPrize(BonusPrize $bonusPrize): PrizeView
    {
        $status = '';
        if ($bonusPrize->getState()->equals(BonusPrizeState::AVAILABLE())) {
            $status = 'Доступно';
        }

        if ($bonusPrize->getState()->equals(BonusPrizeState::ACCEPTED())) {
            $status = 'Зачислено';
        }

        if ($bonusPrize->getState()->equals(BonusPrizeState::DECLINED())) {
            $status = 'Отклонено клиентом';
        }

        return new PrizeView(
            $bonusPrize->getId(),
            sprintf('Бонусный приз размером в %s баллов', $bonusPrize->getAmount()),
            $status,
            'bonus',
            false,
            false,
            false,
            $bonusPrize->isCanAccept(),
            $bonusPrize->isCanDecline()
        );
    }

    public function createByMoneyPrize(MoneyPrize $moneyPrize): PrizeView
    {
        $status = '';
        if ($moneyPrize->getState()->equals(MoneyPrizeState::AVAILABLE())) {
            $status = 'Доступно';
        }

        if ($moneyPrize->getState()->equals(MoneyPrizeState::WAIT_FOR_TRANSFER())) {
            $status = 'Отправляется в банк';
        }

        if ($moneyPrize->getState()->equals(MoneyPrizeState::TRANSFERRED())) {
            $status = 'Отправлено в банк';
        }

        if ($moneyPrize->getState()->equals(MoneyPrizeState::CONVERTED_TO_BONUS())) {
            $status = 'Сконвертировано в бонусы';
        }

        if ($moneyPrize->getState()->equals(MoneyPrizeState::DECLINED())) {
            $status = 'Отклонено клиентом';
        }

        return new PrizeView(
            $moneyPrize->getId(),
            sprintf('Денежный приз размером в %s у.е.', $moneyPrize->getAmount()),
            $status,
            'money',
            $moneyPrize->isCanSendMoneyToBank(),
            $moneyPrize->isCanConvertMoneyToBonus(),
            false,
            false,
            $moneyPrize->isCanDecline()
        );
    }

    public function createByThingPrize(ThingPrize $thingPrize): PrizeView
    {
        $status = '';
        if ($thingPrize->getState()->equals(ThingPrizeState::AVAILABLE())) {
            $status = 'Доступно';
        }

        if ($thingPrize->getState()->equals(ThingPrizeState::TRANSFERRED())) {
            $status = 'Отправлено почтой';
        }

        if ($thingPrize->getState()->equals(ThingPrizeState::DECLINED())) {
            $status = 'Отклонено клиентом';
        }

        return new PrizeView(
            $thingPrize->getId(),
            sprintf('Приз «%s»', $thingPrize->getTitle()),
            $status,
            'thing',
            false,
            false,
            $thingPrize->isCanSendToUser(),
            false,
            $thingPrize->isCanDecline()
        );
    }
}
