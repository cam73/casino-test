<?php

namespace Application\Feature\Prize\UseCase\ShowList\View;

use Application\Component\Prize\BonusPrize\BonusPrize;
use Application\Component\Prize\BonusPrize\BonusPrizeState;
use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Prize\ThingPrize\ThingPrize;

class PrizeListViewFactory
{
    private PrizeViewFactory $prizeViewFactory;

    public function __construct(PrizeViewFactory $prizeViewFactory)
    {
        $this->prizeViewFactory = $prizeViewFactory;
    }

    /**
     * @param array|BonusPrize[] $bonusPrizes
     * @param array|MoneyPrize[] $moneyPrizes
     * @param array|ThingPrize[] $thingPrizes
     * @return PrizeListView
     */
    public function create(array $bonusPrizes, array $moneyPrizes, array $thingPrizes): PrizeListView
    {
        $prizes = [];

        foreach ($bonusPrizes as $bonusPrize) {
            $prizes[] = $this->prizeViewFactory->createByBonusPrize($bonusPrize);
        }

        foreach ($moneyPrizes as $moneyPrize) {
            $prizes[] = $this->prizeViewFactory->createByMoneyPrize($moneyPrize);
        }

        foreach ($thingPrizes as $thingPrize) {
            $prizes[] = $this->prizeViewFactory->createByThingPrize($thingPrize);
        }

        return new PrizeListView($prizes);
    }
}
