<?php

namespace Application\Feature\Prize\UseCase\ShowList\View;

class PrizeView
{
    private string $id;

    private string $title;

    private string $status;

    private bool $sendMoneyToBank;

    private bool $convertMoneyToBonus;

    private bool $sendThingToUser;

    private bool $acceptBonus;

    private bool $declinePrize;

    private string $type;

    public function __construct(
        string $id,
        string $title,
        string $status,
        string $type,
        bool $sendMoneyToBank,
        bool $convertMoneyToBonus,
        bool $sendThingToUser,
        bool $acceptBonus,
        bool $declinePrize
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->status = $status;
        $this->type = $type;
        $this->sendMoneyToBank = $sendMoneyToBank;
        $this->convertMoneyToBonus = $convertMoneyToBonus;
        $this->sendThingToUser = $sendThingToUser;
        $this->acceptBonus = $acceptBonus;
        $this->declinePrize = $declinePrize;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isSendMoneyToBank(): bool
    {
        return $this->sendMoneyToBank;
    }

    public function isConvertMoneyToBonus(): bool
    {
        return $this->convertMoneyToBonus;
    }

    public function isSendThingToUser(): bool
    {
        return $this->sendThingToUser;
    }

    public function isAcceptBonus(): bool
    {
        return $this->acceptBonus;
    }

    public function isDeclinePrize(): bool
    {
        return $this->declinePrize;
    }
}
