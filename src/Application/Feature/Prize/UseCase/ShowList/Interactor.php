<?php

namespace Application\Feature\Prize\UseCase\ShowList;

use Application\Component\Storage\PrizeStorageInterface;
use Application\Feature\Prize\UseCase\ShowList\View\PrizeListView;
use Application\Feature\Prize\UseCase\ShowList\View\PrizeListViewFactory;

class Interactor
{
    private PrizeStorageInterface $prizeStorage;

    private PrizeListViewFactory $prizeListViewFactory;

    public function __construct(
        PrizeStorageInterface $prizeStorage,
        PrizeListViewFactory $prizeListViewFactory
    ) {
        $this->prizeStorage = $prizeStorage;
        $this->prizeListViewFactory = $prizeListViewFactory;
    }

    public function getView(string $clientId): PrizeListView
    {
        $bonusPrizes = $this->prizeStorage->getClientBonusPrizes($clientId);
        $moneyPrizes = $this->prizeStorage->getClientMoneyPrizes($clientId);
        $thingPrizes = $this->prizeStorage->getClientThingPrizes($clientId);

        return $this->prizeListViewFactory->create($bonusPrizes, $moneyPrizes, $thingPrizes);
    }
}
