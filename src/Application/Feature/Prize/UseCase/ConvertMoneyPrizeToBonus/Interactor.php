<?php

namespace Application\Feature\Prize\UseCase\ConvertMoneyPrizeToBonus;

use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Prize\MoneyPrize\MoneyPrizeState;
use Application\Component\Prize\MoneyPrize\MoneyToBonusConverter;
use Application\Component\Storage\ConvertMoneyPrizeToBonusStorageInterface;

class Interactor
{
    private ConvertMoneyPrizeToBonusStorageInterface $storage;

    private MoneyToBonusConverter $moneyToBonusConverter;

    public function __construct(
        ConvertMoneyPrizeToBonusStorageInterface $storage,
        MoneyToBonusConverter $moneyToBonusConverter
    )
    {
        $this->storage = $storage;
        $this->moneyToBonusConverter = $moneyToBonusConverter;
    }

    public function convertMoneyPrizeToBonus(string $moneyPrizeId)
    {
        /** @var MoneyPrize $moneyPrize */
        $moneyPrize = $this->storage->findMoneyPrize($moneyPrizeId);

        if (!$moneyPrize->isCanConvertMoneyToBonus()) {
            throw new ConvertMoneyPrizeToBonusException();
        }

        $bonusAmount = $this->moneyToBonusConverter->convert($moneyPrize->getAmount());

        $moneyPrize->setState(MoneyPrizeState::CONVERTED_TO_BONUS());

        $this->storage->saveConvertMoneyPrizeToBonusResult($moneyPrize, $bonusAmount);
    }
}
