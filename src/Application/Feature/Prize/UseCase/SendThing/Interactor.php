<?php

namespace Application\Feature\Prize\UseCase\SendThing;

use Application\Component\Prize\ThingPrize\ThingPrize;
use Application\Component\Prize\ThingPrize\ThingPrizeState;
use Application\Component\Storage\PrizeStorageInterface;

class Interactor
{
    private PrizeStorageInterface $prizeStorage;

    public function __construct(
        PrizeStorageInterface $prizeStorage
    ) {
        $this->prizeStorage = $prizeStorage;
    }

    public function sendThingPrize(string $thingPrizeId)
    {
        /** @var ThingPrize $thingPrize */
        $thingPrize = $this->prizeStorage->findPrize(ThingPrize::class, $thingPrizeId);

        if (!$thingPrize->isCanSendToUser()) {
            throw new SendThingException();
        }

        // @todo обратиться к внешнему сервису, ответственному за создание заявок на отправку товаров

        $thingPrize->setState(ThingPrizeState::TRANSFERRED());

        $this->prizeStorage->saveSendThingPrizeResult($thingPrize);
    }
}
