<?php

namespace Application\Feature\Prize\UseCase\TransferMoney;

use Application\Component\Bank\BankTransactionStatus;
use Application\Component\Bank\BankTransferProviderInterface;
use Application\Component\Prize\MoneyPrize\MoneyPrizeState;
use Application\Component\Storage\TransferMoneyPrizeStorageInterface;
use Application\Feature\Prize\UseCase\ScheduleTransferMoney\TransferMoneyPrizeException;

class Interactor
{
    private TransferMoneyPrizeStorageInterface $storage;

    private BankTransferProviderInterface $bankTransferProvider;

    public function __construct(
        TransferMoneyPrizeStorageInterface $storage,
        BankTransferProviderInterface $bankTransferProvider
    ) {
        $this->storage = $storage;
        $this->bankTransferProvider = $bankTransferProvider;
    }

    public function transferMoney(string $bankTransactionId)
    {
        $bankTransaction = $this->storage->getBankTransaction($bankTransactionId);

        if (!$bankTransaction->isCanSendMoneyToBank()) {
            throw new TransferMoneyPrizeException();
        }

        $bankTransfer = $this->bankTransferProvider->doTransfer($bankTransaction);

        if (!$bankTransfer->isSuccessfulState()) {
            throw new TransferMoneyPrizeException();
        }

        $bankTransaction->setStatus(BankTransactionStatus::SENT());
        $bankTransaction->getMoneyPrize()->setState(MoneyPrizeState::TRANSFERRED());

        $this->storage->saveTransferResult($bankTransaction);
    }
}
