<?php

namespace Application\Feature\Prize\UseCase\GiveRandomPrize\View;

use Application\Component\Prize\BonusPrize\BonusPrize;
use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Prize\PrizeInterface;
use Application\Component\Prize\ThingPrize\ThingPrize;

class PrizeViewFactory
{
    public function create(PrizeInterface $prize): PrizeView
    {
        $title = '';

        /** @var BonusPrize $prize */
        if ($prize instanceof BonusPrize) {
            $title = sprintf('Бонусный приз размером в %s баллов', $prize->getAmount());
        }

        /** @var MoneyPrize $prize */
        if ($prize instanceof MoneyPrize) {
            $title = sprintf('Денежный приз размером в %s у.е.', $prize->getAmount());
        }

        /** @var ThingPrize $prize */
        if ($prize instanceof ThingPrize) {
            $title = sprintf('Приз «%s»', $prize->getTitle());
        }

        return new PrizeView($title);
    }
}
