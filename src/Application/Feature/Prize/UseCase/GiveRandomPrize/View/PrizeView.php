<?php

namespace Application\Feature\Prize\UseCase\GiveRandomPrize\View;

class PrizeView
{
    private string $title;

    public function __construct(string $title)
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
