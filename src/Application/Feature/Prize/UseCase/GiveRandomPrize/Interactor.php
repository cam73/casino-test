<?php

namespace Application\Feature\Prize\UseCase\GiveRandomPrize;

use Application\Component\Storage\PrizeStorageInterface;
use Application\Feature\Prize\UseCase\GiveRandomPrize\View\PrizeView;
use Application\Feature\Prize\UseCase\GiveRandomPrize\View\PrizeViewFactory;

class Interactor
{
    private PrizeStorageInterface $prizeStorage;

    private PrizeViewFactory $prizeViewFactory;

    public function __construct(PrizeStorageInterface $prizeStorage, PrizeViewFactory $prizeViewFactory)
    {
        $this->prizeStorage = $prizeStorage;
        $this->prizeViewFactory = $prizeViewFactory;
    }

    public function giveRandomPrize(string $clientId): PrizeView
    {
        return $this->prizeViewFactory->create(
            $this->prizeStorage->giveRandomPrize($clientId)
        );
    }
}
