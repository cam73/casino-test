<?php

namespace Application\Feature\Prize\UseCase\RefillBonus;

use Application\Component\Prize\BonusPrize\BonusPrize;
use Application\Component\Prize\BonusPrize\BonusPrizeState;
use Application\Component\Storage\PrizeStorageInterface;

class Interactor
{
    private PrizeStorageInterface $prizeStorage;

    public function __construct(
        PrizeStorageInterface $prizeStorage
    ) {
        $this->prizeStorage = $prizeStorage;
    }

    public function refillBonus(string $bonusPrizeId)
    {
        /** @var BonusPrize $bonusPrize */
        $bonusPrize = $this->prizeStorage->findPrize(BonusPrize::class, $bonusPrizeId);

        if (!$bonusPrize->isCanDecline()) {
            throw new RefillBonusException();
        }

        $bonusPrize->setState(BonusPrizeState::ACCEPTED());

        $this->prizeStorage->saveRefillBonusResult($bonusPrize);
    }
}
