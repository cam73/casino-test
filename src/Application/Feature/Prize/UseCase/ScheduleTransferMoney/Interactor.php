<?php

namespace Application\Feature\Prize\UseCase\ScheduleTransferMoney;

use Application\Component\Bank\BankTransaction;
use Application\Component\Bank\BankTransactionStatus;
use Application\Component\Bank\BankUserAccountFactory;
use Application\Component\Prize\MoneyPrize\MoneyPrizeState;
use Application\Component\Storage\TransferMoneyPrizeStorageInterface;

class Interactor
{
    private TransferMoneyPrizeStorageInterface $storage;

    private BankUserAccountFactory $bankUserAccountFactory;

    public function __construct(
        TransferMoneyPrizeStorageInterface $storage,
        BankUserAccountFactory $bankUserAccountFactory
    ) {
        $this->storage = $storage;
        $this->bankUserAccountFactory = $bankUserAccountFactory;
    }

    public function scheduleTransferMoney(
        string $moneyPrizeId,
        string $payeeBankAccountTin,
        string $payeeAccountNumber
    )
    {
        $moneyPrize = $this->storage->getMoneyPrize($moneyPrizeId);

        if (!$moneyPrize->isCanSendMoneyToBank()) {
            throw new ScheduleTransferMoneyPrizeException();
        }

        $bankUserAccount = $this->bankUserAccountFactory->create($payeeBankAccountTin, $payeeAccountNumber);

        $moneyPrize->setState(MoneyPrizeState::WAIT_FOR_TRANSFER());

        $bankTransaction = new BankTransaction(
            null,
            $moneyPrize,
            $bankUserAccount,
            BankTransactionStatus::PENDING(),
            $moneyPrize->getAmount()
        );

        $this->storage->saveScheduleResult($moneyPrize, $bankTransaction);
    }
}
