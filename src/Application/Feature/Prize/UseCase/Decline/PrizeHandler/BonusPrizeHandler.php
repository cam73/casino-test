<?php

namespace Application\Feature\Prize\UseCase\Decline\PrizeHandler;

use Application\Component\Prize\BonusPrize\BonusPrize;
use Application\Component\Prize\BonusPrize\BonusPrizeState;
use Application\Component\Prize\PrizeInterface;
use Application\Feature\Prize\UseCase\Decline\DeclinePrizeException;
use Assert\Assertion;
use Assert\AssertionFailedException;

class BonusPrizeHandler implements PrizeHandlerInterface
{
    private const supportClass = BonusPrize::class;

    public function isSupportClass(string $className): bool
    {
        return $className == self::supportClass;
    }

    public function isSupportPrizeType(string $prizeType): bool
    {
        return $prizeType == 'bonus';
    }

    public function getSupportClass(): string
    {
        return self::supportClass;
    }

    /**
     * @param PrizeInterface|BonusPrize $bonusPrize
     */
    public function handle(PrizeInterface $bonusPrize)
    {
        if (!$bonusPrize->isCanDecline()) {
            throw new DeclinePrizeException();
        }

        $bonusPrize->setState(BonusPrizeState::DECLINED());
    }
}
