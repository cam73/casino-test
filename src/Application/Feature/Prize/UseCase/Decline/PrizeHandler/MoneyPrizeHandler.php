<?php

namespace Application\Feature\Prize\UseCase\Decline\PrizeHandler;

use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Prize\MoneyPrize\MoneyPrizeState;
use Application\Component\Prize\PrizeInterface;
use Application\Feature\Prize\UseCase\Decline\DeclinePrizeException;
use Assert\Assertion;
use Assert\AssertionFailedException;

class MoneyPrizeHandler implements PrizeHandlerInterface
{
    private const supportClass = MoneyPrize::class;

    public function isSupportClass(string $className): bool
    {
        return $className == self::supportClass;
    }

    public function isSupportPrizeType(string $prizeType): bool
    {
        return $prizeType == 'money';
    }

    public function getSupportClass(): string
    {
        return self::supportClass;
    }

    /**
     * @param PrizeInterface|MoneyPrize $moneyPrize
     */
    public function handle(PrizeInterface $moneyPrize)
    {
        if (!$moneyPrize->isCanDecline()) {
            throw new DeclinePrizeException();
        }

        $moneyPrize->setState(MoneyPrizeState::DECLINED());
    }
}
