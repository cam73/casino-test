<?php

namespace Application\Feature\Prize\UseCase\Decline\PrizeHandler;

use Application\Component\Prize\PrizeInterface;

interface PrizeHandlerInterface
{
    public function isSupportClass(string $className): bool;

    public function isSupportPrizeType(string $prizeType): bool;

    public function getSupportClass(): string;

    public function handle(PrizeInterface $prize);
}
