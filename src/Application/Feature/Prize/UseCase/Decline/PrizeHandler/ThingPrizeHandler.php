<?php

namespace Application\Feature\Prize\UseCase\Decline\PrizeHandler;

use Application\Component\Prize\PrizeInterface;
use Application\Component\Prize\ThingPrize\ThingPrize;
use Application\Component\Prize\ThingPrize\ThingPrizeState;
use Application\Feature\Prize\UseCase\Decline\DeclinePrizeException;
use Assert\Assertion;
use Assert\AssertionFailedException;

class ThingPrizeHandler implements PrizeHandlerInterface
{
    private const supportClass = ThingPrize::class;

    public function isSupportClass(string $className): bool
    {
        return $className == self::supportClass;
    }

    public function isSupportPrizeType(string $prizeType): bool
    {
        return $prizeType == 'thing';
    }

    public function getSupportClass(): string
    {
        return self::supportClass;
    }

    /**
     * @param PrizeInterface|ThingPrize $thingPrize
     */
    public function handle(PrizeInterface $thingPrize)
    {
        if (!$thingPrize->isCanDecline()) {
            throw new DeclinePrizeException();
        }

        $thingPrize->setState(ThingPrizeState::DECLINED());
    }
}
