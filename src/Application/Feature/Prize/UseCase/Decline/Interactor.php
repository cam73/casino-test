<?php

namespace Application\Feature\Prize\UseCase\Decline;

use Application\Component\Prize\PrizeInterface;
use Application\Component\Storage\PrizeStorageInterface;
use Application\Feature\Prize\UseCase\Decline\PrizeHandler\BonusPrizeHandler;
use Application\Feature\Prize\UseCase\Decline\PrizeHandler\MoneyPrizeHandler;
use Application\Feature\Prize\UseCase\Decline\PrizeHandler\PrizeHandlerInterface;
use Application\Feature\Prize\UseCase\Decline\PrizeHandler\ThingPrizeHandler;

class Interactor
{
    private PrizeStorageInterface $prizeStorage;

    /** @var PrizeHandlerInterface[]  */
    private array $prizeHandlers;

    public function __construct(
        PrizeStorageInterface $prizeStorage,
        BonusPrizeHandler $bonusPrizeHandler,
        MoneyPrizeHandler $moneyPrizeHandler,
        ThingPrizeHandler $thingPrizeHandler
    ) {
        $this->prizeStorage = $prizeStorage;
        $this->prizeHandlers = [$bonusPrizeHandler, $moneyPrizeHandler, $thingPrizeHandler];
    }


    public function declinePrize(string $prizeType, string $prizeId)
    {
        $prize = $this->prizeStorage->findPrize($this->resolvePrizeClass($prizeType), $prizeId);
        $this->handlePrize($prize);

        $this->prizeStorage->saveDeclineResult($prize);
    }

    private function handlePrize(PrizeInterface $prize)
    {
        foreach ($this->prizeHandlers as $prizeHandler) {
            if ($prizeHandler->isSupportClass(get_class($prize))) {
                $prizeHandler->handle($prize);
            }
        }
    }

    private function resolvePrizeClass(string $prizeType): string
    {
        foreach ($this->prizeHandlers as $prizeHandler) {
            if ($prizeHandler->isSupportPrizeType($prizeType)) {
                return $prizeHandler->getSupportClass();
            }
        }
    }
}
