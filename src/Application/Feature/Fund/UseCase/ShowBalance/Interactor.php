<?php

namespace Application\Feature\Fund\UseCase\ShowBalance;

use Application\Component\Storage\FundStorageInterface;
use Application\Feature\Fund\UseCase\ShowBalance\View\BalanceView;
use Application\Feature\Fund\UseCase\ShowBalance\View\ThingView;

class Interactor
{
    private FundStorageInterface $fundStorage;

    public function __construct(FundStorageInterface $fundStorage)
    {
        $this->fundStorage = $fundStorage;
    }

    public function getView(): BalanceView
    {
        $fundMoneyBalance = $this->fundStorage->getFundMoneyBalanceAmount();
        $fundThings = $this->fundStorage->getFundThings();

        $decodedFundThings = [];
        foreach ($fundThings as $fundThing) {
            $decodedFundThings[] = new ThingView($fundThing->getTitle());
        }
        return new BalanceView($fundMoneyBalance, $decodedFundThings);
    }
}
