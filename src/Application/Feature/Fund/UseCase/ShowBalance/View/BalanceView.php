<?php

namespace Application\Feature\Fund\UseCase\ShowBalance\View;

class BalanceView
{
    private string $fundMoneyBalance;

    /** @var array|ThingView[] */
    private array $fundThings;

    /**
     * @param string $fundMoneyBalance
     * @param ThingView[]|array $fundThings
     */
    public function __construct(string $fundMoneyBalance, array $fundThings)
    {
        $this->fundMoneyBalance = $fundMoneyBalance;
        $this->fundThings = $fundThings;
    }

    public function getFundMoneyBalance(): string
    {
        return $this->fundMoneyBalance;
    }

    /**
     * @return ThingView[]|array
     */
    public function getFundThings(): array
    {
        return $this->fundThings;
    }
}
