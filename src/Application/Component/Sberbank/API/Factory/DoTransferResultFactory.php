<?php

namespace Application\Component\Sberbank\API\Factory;

use Application\Component\Sberbank\API\DTO\DoTransferResult;
use Application\Component\Sberbank\API\DTO\TransferStatus;

class DoTransferResultFactory
{
    public function create(string $encodedString): DoTransferResult
    {
        $decodedData = json_decode($encodedString);

        // validation
        // domain exceptions
        // resolve to DTO

        $status = strtoupper($decodedData->status);
        return new DoTransferResult($decodedData->transactionId, TransferStatus::$status());
    }
}
