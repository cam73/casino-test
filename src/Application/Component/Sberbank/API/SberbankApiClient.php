<?php

namespace Application\Component\Sberbank\API;

use Application\Component\Sberbank\API\DTO\DoTransferParams;
use Application\Component\Sberbank\API\DTO\DoTransferResult;
use Application\Component\Sberbank\API\Factory\DoTransferResultFactory;
use Psr\Http\Client\ClientInterface;

class SberbankApiClient
{
    private ClientInterface $httpClient;

    private DoTransferResultFactory $doTransferResultFactory;

    public function __construct(ClientInterface $httpClient, DoTransferResultFactory $doTransferResultFactory)
    {
        $this->httpClient = $httpClient;
        $this->doTransferResultFactory = $doTransferResultFactory;
    }

    /**
     * @param DoTransferParams $params
     * @return DoTransferResult
     */
    public function doTransfer(DoTransferParams $params): DoTransferResult
    {
        // $request = new Request();
        // $response = $this->httpClient->sendRequest($request);
        // $body = $response->getBody();
        $body = '{"transactionId":777,"status":"done"}';

        return $this->doTransferResultFactory->create($body);
    }
}
