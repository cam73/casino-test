<?php

namespace Application\Component\Sberbank\API\DTO;

class DoTransferResult
{
    private string $id;

    private TransferStatus $status;

    public function __construct(string $id, TransferStatus $status)
    {
        $this->id = $id;
        $this->status = $status;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getStatus(): TransferStatus
    {
        return $this->status;
    }
}
