<?php

namespace Application\Component\Sberbank\API\DTO;

use MyCLabs\Enum\Enum;

/**
 * @method static self PENDING()
 * @method static self DONE()
 * @method static self ERROR()
 */
class TransferStatus extends Enum
{
    private const PENDING = 'pending';
    private const DONE = 'done';
    private const ERROR = 'error';
}
