<?php

namespace Application\Component\Sberbank\API\DTO;

class DoTransferParams
{
    private string $payerAccountNumber;

    private string $payeeBankTin;

    private string $payeeAccountNumber;

    private string $amount;

    public function __construct(
        string $sourceAccountNumber,
        string $targetBankTin,
        string $targetAccountNumber,
        string $amount
    ) {
        $this->payerAccountNumber = $sourceAccountNumber;
        $this->payeeBankTin = $targetBankTin;
        $this->payeeAccountNumber = $targetAccountNumber;
        $this->amount = $amount;
    }

    public function getPayerAccountNumber(): string
    {
        return $this->payerAccountNumber;
    }

    public function getPayeeBankTin(): string
    {
        return $this->payeeBankTin;
    }

    public function getPayeeAccountNumber(): string
    {
        return $this->payeeAccountNumber;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }
}
