<?php

namespace Application\Component\Sberbank;

use Application\Component\Bank\BankTransaction;
use Application\Component\Bank\BankTransactionStatus;
use Application\Component\Bank\BankTransfer;
use Application\Component\Bank\BankTransferProviderInterface;
use Application\Component\Sberbank\API\DTO\DoTransferParams;
use Application\Component\Sberbank\API\SberbankApiClient;
use Assert\Assertion;
use Assert\AssertionFailedException;

class SberbankProvider implements BankTransferProviderInterface
{
    private string $payerAccountNumber;

    private SberbankApiClient $apiClient;

    private TransferStatusResolver $transferStatusResolver;

    public function __construct(string $payerAccountNumber, SberbankApiClient $apiClient, TransferStatusResolver $transferStatusResolver)
    {
        $this->payerAccountNumber = $payerAccountNumber;
        $this->apiClient = $apiClient;
        $this->transferStatusResolver = $transferStatusResolver;
    }

    /**
     * @param BankTransaction $bankTransaction
     * @return BankTransfer
     * @throws AssertionFailedException
     */
    public function doTransfer(BankTransaction $bankTransaction): BankTransfer
    {
        Assertion::true($bankTransaction->getStatus()->equals(BankTransactionStatus::PENDING()));

        $result = $this->apiClient->doTransfer(
            new DoTransferParams(
                $this->payerAccountNumber,
                $bankTransaction->getBankUserAccount()->getBank()->getTin(),
                $bankTransaction->getBankUserAccount()->getAccountNumber(),
                $bankTransaction->getAmount()
            )
        );

        return new BankTransfer($result->getId(), $this->transferStatusResolver->resolve($result->getStatus()));
    }
}
