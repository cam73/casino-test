<?php

namespace Application\Component\Sberbank;

use Application\Component\Bank\BankTransferState;
use Application\Component\Sberbank\API\DTO\TransferStatus;

class TransferStatusResolver
{
    public function resolve(TransferStatus $transferStatus): BankTransferState
    {
        if ($transferStatus->equals(TransferStatus::PENDING())) {
            return BankTransferState::REGISTERED();
        }

        if ($transferStatus->equals(TransferStatus::DONE())) {
            return BankTransferState::DONE();
        }

        if ($transferStatus->equals(TransferStatus::ERROR())) {
            return BankTransferState::ERROR();
        }
    }
}
