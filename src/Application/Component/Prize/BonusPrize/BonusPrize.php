<?php

namespace Application\Component\Prize\BonusPrize;

use Application\Component\Prize\Prize;
use Application\Component\Prize\PrizeInterface;

class BonusPrize implements PrizeInterface
{
    private string $amount;

    private PrizeInterface $prize;

    private BonusPrizeState $state;

    public function __construct(string $id, string $amount, BonusPrizeState $state)
    {
        $this->prize = new Prize($id);
        $this->amount = $amount;
        $this->state = $state;
    }

    public function getId(): string
    {
        return $this->prize->getId();
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function getState(): BonusPrizeState
    {
        return $this->state;
    }

    /**
     * @param BonusPrizeState $state
     */
    public function setState(BonusPrizeState $state): void
    {
        $this->state = $state;
    }

    public function isCanAccept(): bool
    {
        return $this->state->equals(BonusPrizeState::AVAILABLE());
    }

    public function isCanDecline(): bool
    {
        return $this->state->equals(BonusPrizeState::AVAILABLE());
    }
}
