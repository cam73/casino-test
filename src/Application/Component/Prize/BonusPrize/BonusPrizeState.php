<?php

namespace Application\Component\Prize\BonusPrize;

use MyCLabs\Enum\Enum;

/**
 * @method static self AVAILABLE()
 * @method static self ACCEPTED()
 * @method static self DECLINED()
 */
class BonusPrizeState extends Enum
{
    private const AVAILABLE = 'available';
    private const ACCEPTED = 'accepted';
    private const DECLINED = 'declined';
}
