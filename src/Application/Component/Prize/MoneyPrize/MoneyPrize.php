<?php

namespace Application\Component\Prize\MoneyPrize;

use Application\Component\Prize\Prize;
use Application\Component\Prize\PrizeInterface;

class MoneyPrize implements PrizeInterface
{
    private string $amount;

    private PrizeInterface $prize;

    private MoneyPrizeState $state;

    public function __construct(string $id, string $amount, MoneyPrizeState $state)
    {
        $this->prize = new Prize($id);
        $this->amount = $amount;
        $this->state = $state;
    }

    public function getId(): string
    {
        return $this->prize->getId();
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function getState(): MoneyPrizeState
    {
        return $this->state;
    }

    public function setState(MoneyPrizeState $state): void
    {
        $this->state = $state;
    }

    public function isCanSendMoneyToBank(): bool
    {
        return $this->isAvailableState();
    }

    public function isCanConvertMoneyToBonus(): bool
    {
        return $this->isAvailableState();
    }

    public function isCanDecline(): bool
    {
        return $this->isAvailableState();
    }

    private function isAvailableState(): bool
    {
        return $this->state->equals(MoneyPrizeState::AVAILABLE());
    }
}
