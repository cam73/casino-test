<?php

namespace Application\Component\Prize\MoneyPrize;

use Assert\Assertion;
use Assert\AssertionFailedException;

class MoneyToBonusConverter
{
    private int $moneyPerOneBonus;

    /**
     * MoneyToBonusConverter constructor.
     * @param int $moneyPerOneBonus
     * @throws AssertionFailedException
     */
    public function __construct(int $moneyPerOneBonus)
    {
        Assertion::greaterThan($moneyPerOneBonus, 0);

        $this->moneyPerOneBonus = $moneyPerOneBonus;
    }

    /**
     * @param string $moneyAmount
     * @return string
     * @throws AssertionFailedException
     */
    public function convert(string $moneyAmount): string
    {
        Assertion::greaterThan($moneyAmount, 0);

        return round($moneyAmount / $this->moneyPerOneBonus);
    }
}
