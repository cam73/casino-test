<?php

namespace Application\Component\Prize\MoneyPrize;

use MyCLabs\Enum\Enum;

/**
 * @method static self AVAILABLE()
 * @method static self WAIT_FOR_TRANSFER()
 * @method static self TRANSFERRED()
 * @method static self CONVERTED_TO_BONUS()
 * @method static self DECLINED()
 */
class MoneyPrizeState extends Enum
{
    private const AVAILABLE = 'available';
    private const WAIT_FOR_TRANSFER = 'wait_for_transfer';
    private const TRANSFERRED = 'transferred';
    private const CONVERTED_TO_BONUS = 'converted_to_bonus';
    private const DECLINED = 'declined';
}
