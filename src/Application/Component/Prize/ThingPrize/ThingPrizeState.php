<?php

namespace Application\Component\Prize\ThingPrize;

use MyCLabs\Enum\Enum;

/**
 * @method static self AVAILABLE()
 * @method static self TRANSFERRED()
 * @method static self DECLINED()
 */
class ThingPrizeState extends Enum
{
    private const AVAILABLE = 'available';
    private const TRANSFERRED = 'transferred';
    private const DECLINED = 'declined';
}
