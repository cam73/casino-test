<?php

namespace Application\Component\Prize\ThingPrize;

use Application\Component\Prize\Prize;
use Application\Component\Prize\PrizeInterface;

class ThingPrize implements PrizeInterface
{
    private string $title;

    private PrizeInterface $prize;

    private ThingPrizeState $state;

    public function __construct(string $id, string $title, ThingPrizeState $state)
    {
        $this->prize = new Prize($id);
        $this->title = $title;
        $this->state = $state;
    }

    public function getId(): string
    {
        return $this->prize->getId();
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getState(): ThingPrizeState
    {
        return $this->state;
    }

    public function setState(ThingPrizeState $state): void
    {
        $this->state = $state;
    }

    public function isCanSendToUser(): bool
    {
        return $this->state->equals(ThingPrizeState::AVAILABLE());
    }

    public function isCanDecline(): bool
    {
        return $this->state->equals(ThingPrizeState::AVAILABLE());
    }
}
