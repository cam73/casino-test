<?php

namespace Application\Component\Prize;

interface PrizeProviderInterface
{
    public function isSupportClass(string $className): bool;

    /**
     * @param string $clientId
     * @return PrizeInterface[]
     */
    public function getClientPrizes(string $clientId): array;

    /**
     * @param string $clientId
     * @return PrizeInterface
     */
    public function giveRandomPrizeToClient(string $clientId): PrizeInterface;

    public function canGivePrize(): bool;

    public function getPrize(string $prizeId): PrizeInterface;

    public function saveDeclineResult(PrizeInterface $prize);
}
