<?php

namespace Application\Component\Prize;

interface PrizeProviderResolverInterface
{
    public function resolveByClass(string $className): PrizeProviderInterface;

    public function getRandomProviderForGivePrize(): PrizeProviderInterface;
}
