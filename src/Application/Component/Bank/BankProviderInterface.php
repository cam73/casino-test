<?php

namespace Application\Component\Bank;

interface BankProviderInterface {
    public function transfer(BankTransaction $bankTransaction): BankTransferState;
}
