<?php

namespace Application\Component\Bank;

use MyCLabs\Enum\Enum;

/**
 * @method static self REGISTERED()
 * @method static self DONE()
 * @method static self ERROR()
 */
class BankTransferState extends Enum
{
    private const REGISTERED = 'registered';
    private const DONE = 'done';
    private const ERROR = 'error';
}
