<?php

namespace Application\Component\Bank;

class BankAccount
{
    private string $tin;

    public function __construct(string $tin)
    {
        $this->tin = $tin;
    }

    public function getTin(): string
    {
        return $this->tin;
    }
}
