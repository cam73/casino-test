<?php

namespace Application\Component\Bank;

class BankUserAccount
{
    private BankAccount $bank;

    private string $accountNumber;

    public function __construct(BankAccount $bank, string $accountNumber)
    {
        $this->bank = $bank;
        $this->accountNumber = $accountNumber;
    }

    public function getBank(): BankAccount
    {
        return $this->bank;
    }

    public function getAccountNumber(): string
    {
        return $this->accountNumber;
    }
}
