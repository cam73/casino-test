<?php

namespace Application\Component\Bank;

use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Storage\StorageEntity;
use Application\Component\Storage\StorageEntityInterface;

class BankTransaction implements StorageEntityInterface
{
    private StorageEntityInterface $storageEntity;

    private MoneyPrize $moneyPrize;

    private BankUserAccount $bankUserAccount;

    private BankTransactionStatus $status;

    private string $amount;

    public function __construct(
        ?string $id,
        MoneyPrize $moneyPrize,
        BankUserAccount $bankUserAccount,
        BankTransactionStatus $status,
        string $amount
    ) {
        $this->storageEntity = new StorageEntity($id);
        $this->moneyPrize = $moneyPrize;
        $this->bankUserAccount = $bankUserAccount;
        $this->status = $status;
        $this->amount = $amount;
    }

    public function getBankUserAccount(): BankUserAccount
    {
        return $this->bankUserAccount;
    }

    public function getStatus(): BankTransactionStatus
    {
        return $this->status;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function setStatus(BankTransactionStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function setBankUserAccount(BankUserAccount $bankUserAccount): self
    {
        $this->bankUserAccount = $bankUserAccount;

        return $this;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getMoneyPrize(): MoneyPrize
    {
        return $this->moneyPrize;
    }

    public function setMoneyPrize(MoneyPrize $moneyPrize): self
    {
        $this->moneyPrize = $moneyPrize;

        return $this;
    }

    public function getId(): string
    {
        return $this->storageEntity->getId();
    }

    public function isCanSendMoneyToBank(): bool
    {
        return $this->status->equals(BankTransactionStatus::PENDING());
    }
}
