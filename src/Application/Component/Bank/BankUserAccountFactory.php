<?php

namespace Application\Component\Bank;

class BankUserAccountFactory
{
    public function create(string $bankAccountTin, string $accountNumber): BankUserAccount
    {
        return new BankUserAccount(new BankAccount($bankAccountTin), $accountNumber);
    }
}
