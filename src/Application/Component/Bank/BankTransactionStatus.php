<?php

namespace Application\Component\Bank;

use MyCLabs\Enum\Enum;

/**
 * @method static self PENDING()
 * @method static self SENT()
 * @method static self CANCELED()
 */
class BankTransactionStatus extends Enum
{
    private const PENDING = 'pending';
    private const SENT = 'sent';
    private const CANCELED = 'canceled';
}
