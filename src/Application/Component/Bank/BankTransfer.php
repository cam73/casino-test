<?php

namespace Application\Component\Bank;

class BankTransfer
{
    private string $id;

    private BankTransferState $state;

    public function __construct(string $id, BankTransferState $status)
    {
        $this->id = $id;
        $this->state = $status;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getState(): BankTransferState
    {
        return $this->state;
    }

    public function isSuccessfulState(): bool
    {
        return (
            $this->state->equals(BankTransferState::REGISTERED()) ||
            $this->state->equals(BankTransferState::DONE())
        );
    }
}
