<?php

namespace Application\Component\Bank;

interface BankTransferProviderInterface
{
    public function doTransfer(BankTransaction $bankTransaction): BankTransfer;
}
