<?php

namespace Application\Component\Storage;

class StorageEntity implements StorageEntityInterface
{
    private ?string $id;

    public function __construct(?string $id)
    {
        $this->id = $id;
    }

    public function getId(): ?string
    {
        return $this->id;
    }
}
