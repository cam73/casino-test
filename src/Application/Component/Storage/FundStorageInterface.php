<?php

namespace Application\Component\Storage;

use Application\Component\Thing\Thing;

interface FundStorageInterface
{
    public function getFundMoneyBalanceAmount(): string;

    /** @return Thing[] */
    public function getFundThings(): array;
}
