<?php

namespace Application\Component\Storage;

use Application\Component\Prize\BonusPrize\BonusPrize;
use Application\Component\Prize\MoneyPrize\MoneyPrize;
use Application\Component\Prize\PrizeInterface;
use Application\Component\Prize\ThingPrize\ThingPrize;

interface PrizeStorageInterface
{
    /**
     * @param string $clientId
     * @return BonusPrize[]
     */
    public function getClientBonusPrizes(string $clientId): array;

    /**
     * @param string $clientId
     * @return MoneyPrize[]
     */
    public function getClientMoneyPrizes(string $clientId): array;

    /**
     * @param string $clientId
     * @return ThingPrize[]
     */
    public function getClientThingPrizes(string $clientId): array;

    public function giveRandomPrize(string $clientId): PrizeInterface;

    public function findPrize(string $prizeClass, string $prizeId): PrizeInterface;

    public function saveDeclineResult(PrizeInterface $prize);

    public function saveRefillBonusResult(BonusPrize $bonusPrize);

    public function saveSendThingPrizeResult(ThingPrize $thingPrize);
}
