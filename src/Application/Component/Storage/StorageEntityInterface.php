<?php

namespace Application\Component\Storage;

interface StorageEntityInterface
{
    public function getId(): ?string;
}
