<?php

namespace Application\Component\Storage;

interface BonusStorageInterface
{
    public function getBonusBalanceAmountByClientId(string $clientId): string;
}
