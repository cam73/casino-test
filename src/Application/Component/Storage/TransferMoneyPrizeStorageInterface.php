<?php

namespace Application\Component\Storage;

use Application\Component\Bank\BankTransaction;
use Application\Component\Prize\MoneyPrize\MoneyPrize;

interface TransferMoneyPrizeStorageInterface
{
    public function getMoneyPrize(string $moneyPrizeId): MoneyPrize;

    public function saveScheduleResult(MoneyPrize $moneyPrize, BankTransaction $bankTransaction): void;

    public function getBankTransaction(string $bankTransactionId): BankTransaction;

    public function saveTransferResult(BankTransaction $bankTransaction): void;
}
