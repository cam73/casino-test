<?php

namespace Application\Component\Storage;

use Application\Component\Prize\MoneyPrize\MoneyPrize;

interface ConvertMoneyPrizeToBonusStorageInterface
{
    public function findMoneyPrize(string $moneyPrizeId): MoneyPrize;

    public function saveConvertMoneyPrizeToBonusResult(MoneyPrize $moneyPrize, string $bonusAmount);
}
